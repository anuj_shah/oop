import java.util.Scanner;

class Counter
{
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);	
		int arr[] = new int[1000000], i, j, a, b, k, l=0, m=0, max, min, no;
		int arr1[] = new int[1000000];
		int count[] = new int[1000000];
		System.out.print("Enter starting point : ");	//to enter starting point of the series
		a = input.nextInt();
		System.out.print("Enter ending point : ");	//to enter ending point of the series
		b = input.nextInt();
		System.out.print("\n");
		for(i=a, j=0; i<=b; i++, j++)			//to store elements in array for displaying afterwards
		{
			arr[j]=i;
		}
		for(i=0; i<j; i++)				//to display the whole no starting from a and ending in b
		{
			System.out.print(arr[i]);
		}
		for(i=a, j=0; i<=b; i++)			//to store elements by separating them
		{
			no=i;
			while(no!=0)
			{
				arr1[j]=no%10;
				no=no/10;
				j++;
			}
		}
		for(k=0; k<j; k++)				//counts repetition of digits
		{
			for(i=0, count[k]=0; i<j; i++)
			{
				if(arr1[k]==arr1[i])
				{
					count[k]++;
				}
			}
		}
		for(i=0, max=count[0]; i<j; i++)		//sees which digit is occuring max times
		{	
			if(count[i]>=max)
			{
				max=count[i];
				l=i;
			}
		}
		for(i=0, min=count[0]; i<j; i++)		//sees which digit is occuring min times
		{	
			if(count[i]<=min)
			{
				min=count[i];
				m=i;
			}
		}
		System.out.print("\n\n");
		System.out.println("Max occuring element is " + arr1[l]);
		System.out.println("It occurs " + count[l] + " times");
		System.out.print("\n");
		System.out.println("Min occuring element is " + arr1[m]);
		System.out.println("It occurs " + count[m] + " times");
		System.out.print("\n");
	}
}
