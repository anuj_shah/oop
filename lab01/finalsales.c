                                        /* Supermart Sales Transaction */

#include<stdio.h>

int main()

{
    int i, j, cid, np, sum, no, k;
    struct customer
    {
        char name[100];
        int id;
        char product[100];
        int rate;
        int qty;
    };
    struct customer srno[100];
    printf("Enter no of customers : ");                     /* No of customers to be entered first */
    scanf("%d", &no);
    printf("\n");
    for(i=0; i<=no-1; i++)                                  /* Give customer name and id's will be allocated automatically */
    {
        printf("Customer Name       : ");
        scanf("%s", srno[i].name);
        srno[i].id = i+1;
        printf("Customer Id         : %d\n", srno[i].id);
        printf("\n");
    }
    for(k=0; ; k++)
    {
    printf("Enter Customer Id : ");                         /* Scans customer id */
    scanf("%d", &cid);
    printf("\n");
    for(i=0; i<=no; i++)
    {

        if(i == cid)                                        /* If id matches then product details have to be entered */
        {
            printf("Customer Name                       : %s\n", srno[cid-1].name);
            printf("\n");
            printf("Enter no of products                : ");
            scanf("%d", &np);
            printf("\n");
            for(j=0, sum=0; j<=np-1; j++)
            {
            printf("Product %d                           : ", j+1);
            scanf("%s", srno[j].product);
            printf("Rate                                : ");
            scanf("%d", &srno[j].rate);
            printf("Qty                                 : ");
            scanf("%d", &srno[j].qty);
            sum+=srno[j].qty * srno[j].rate;
            printf("\n");
            }
            printf("Total Amt to be paid                : %d\n\n", sum);
            printf("Thank you for shopping with us\n\n");
        }
    }
    }
    return 0;
}
