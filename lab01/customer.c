					/* Supermart sales transaction */

#include<stdio.h>

int count[100];

struct product						/* structure of product */
{
	char product[100];
	float rate;
	int qty;
};


struct customer						/* structure of customer */
{
	char name[100];
	int cid;
	struct product p[100];
}c[100];


void add()						/* function for adding records */
{
	char ch;
	int i=0;
	do
	{
		printf("Enter Customer Name : ");	
		scanf("%s", c[i].name);
		printf("Enter Customer ID : ");
		scanf("%d", &c[i].cid);
		printf("\n");
		printf("Do you want to add more data(y/n)? ");
		scanf(" %c", &ch);	
		i++;
	}while(ch!='n');
}

void product()							/* function for adding products to the existing records */
{
	char ch;
	int i=0, j, cid2;
	printf("Enter Customer ID : ");
	scanf("%d", &cid2);
	printf("\n");
	do
	{		
		if(cid2 == c[i].cid)						/* checks whether entered account no matches with existing acc no */
		{
			printf("Customer Name : %s\n", c[i].name);
			for(j=0; ch!='n'; j++)
			{
				printf("Product : ");
				scanf("%s", c[i].p[j].product);
				printf("Rate : ");
				scanf("%f", &c[i].p[j].rate);
				printf("Qty : ");
				scanf("%d", &c[i].p[j].qty);
				printf("\n");
				printf("Do you want to add more product(y/n)? ");
				scanf(" %c", &ch);	
				count[i]++;
			}
		}
		i++;
	}while(ch!='n');
}

void bill()							/* function for generating bill of existing records */
{
	int cid2, i=0, k;
	float sum=0;
	printf("Enter Customer ID : ");
	scanf("%d", &cid2);
	do
	{
		if(cid2 == c[i].cid)
		{
			printf("Customer Name : %s\n", c[i].name);
			for(k=0; k<=count[i]; k++)			/* for calculating total amt to be paid */
			{
				sum = sum + (c[i].p[k].rate * c[i].p[k].qty);
			}
			printf("Total Amt to be paid : %f\n", sum);
			printf("Thank you for shopping with us\n");
		}
	i++;
	}while(cid2 == c[i].cid);
}



int main()

{
	printf("0) For Termination\n");
	printf("1) Add Records\n");
	printf("2) Add Products\n");
	printf("3) Generate Bill\n\n");	
	int x;
	do
	{	
		printf("What do you want to do? ");
		scanf("%d", &x);
		printf("\n\n");
		switch(x)
		{
			case 1:
			add();					/* calling add function */
			break;
			case 2:
			product();				/* calling product function */
			break;
			case 3:
			bill();					/* calling bill function */	
			break;
		}
	}while(x!=0);
	return 0;
}
