import java.util.Scanner;

abstract class Account		//abstract parent class Account
{
	protected double accNo;		//kept protected so that it can be used in it's child class
	protected double balance;
	
	Account(double no, double bal)	//Constructor of Account class
	{
		accNo = no;
		balance = bal;
	}
	
	public boolean deposit(double money, double accountNo)	//deposit member function is common for savings and current therefore it has been kept in Account class
	{
		if(accountNo == accNo)	//first checks account no
		{
			if(money>=0)	//checks money(should be at least 0rs)
			{
				balance += money;
				return true;
			}
			return false;
		}
		return false;
	}
	
	abstract public boolean withdraw(double money, double accountNo);  //only prototype has been defined so that it can be implemented in dynamic polymorphism
	//kept abstract so that any user who would create other child classes of Account has to create this member function and declare it's method
	public void display()	//to display account no and balance and it is common for both child classes therefore it has been kept in parent class
	{
		System.out.println("Acc No: " + accNo);
		System.out.println("Balance: " + balance);
	}
}

class Savings extends Account		//child class of Account
{
	private double totInterest=0;	
	private double interest;
	private float rateOfInterest;
	private int l;
	private int month;
	Savings(double no, double bal, float rate)	//Constructor of child class Savings
	{
		super(no, bal);	//calling Constructor of parent class Account using 'super' keyword
		rate = 3.5f;
	}
	
	public boolean interestCalculate(int mon)	//to calculate interest
	{
		if(mon>0)		//first checks condition on month
		{
			month = mon;
			for(l=1; l<=month; l++)		//loop to calculate interest once in each quarter which has been checked by (l%4==0)
			{
				if(l%3==0)
				{
					interest = ((balance)*(rateOfInterest)*(3))/1200;	//formula of interest (i = prn/1200) where time period is in months
					balance += interest;	//adding interest in balance
					totInterest+=interest;	//calculating total interest
				}
			}
			interest = ((balance)*(rateOfInterest)*(month%3))/1200;	//calculating interest for remaining amt of months
			balance += interest;
			totInterest += interest;
			return true;
		}
		return false;
	}
	
	public double get_interest() //returning total interest
	{
		return totInterest;
	}
	
	public int get_month()	//returning month
	{
		return month;
	}
	
	public boolean set_rate(float rate)	//setting rate of interest
	{
		if(rate>0)
		{	
			rateOfInterest = rate;
			return true;
		}
		return false;
	}
	public float get_rate()		//returning rate of interest
	{
		return rateOfInterest;
	}
	public boolean withdraw(double money, double accountNo)	//withdraw member function for savings account
	{
		if(accountNo == accNo)		//first checks if account no are equal
		{
			if(money>=0 && money<=balance-500)	//since money has to be greater than 0 and there should be min balance of 500 in savings account
			{
				balance -= money;
				return true;
			}
			return false;
		}
		return false;
	}
}

class Current extends Account	//second child class of parent class Account
{
	private double overdraftLimit;
	Current(double no, double bal, double overdraft)	//Constructor of child class Current
	{
		super(no, bal);		//calling parent class Constructor of Account
		overdraft = 500;
	}
	public boolean set_overdraft(double overdraft)		//setting overdraft limit
	{
		if(overdraft>=500 && overdraft<=10000000)	//overdraft limit has to be between "500"rs and "1Cr"rs.
		{
			overdraftLimit = overdraft;
			return true;
		}
		return false;
	}
	public double get_overdraft()		//returning overdraft
	{
		return overdraftLimit;
	}
	public boolean withdraw(double money, double accountNo)	//withdraw member function for Current account
	{
		if(accountNo == accNo)		//checks equality of account no
		{
			if(money>=0 && money<=balance+overdraftLimit)	//customer can withdraw money upto the negative balance beyond the overdraft
			{
				balance -= money;
				return true;
			}
			return false;
		}
		return false;
	}
}



class Banking			//main class
{
	public static void main(String []args)		//main function
	{
		Scanner input = new Scanner(System.in);
		double no=1516000, accountNo, overdraft=0, bal, money;	//here account no has been set to some default value therefore it generates automatic account no for different customers
		int i=0, k, j=0, s1, c1, l, monthX;
		byte flag=1, x1, choice, o;
		float roi=0;
		System.out.print("How many savings accounts do you want to keep? ");
		s1 = input.nextInt();
		System.out.print("How many current accounts do you want to keep? ");
		c1 = input.nextInt();
		Savings []s = new Savings[s1];		//creating reference of array of objects for Savings account
		Current []c = new Current[c1];
		Account []a = new Account[s1+c1];	//since Account is an abstract class therfore only it's reference can be created and we can't instantiate it
		do
		{
			System.out.print("0. TERMINATE \n1. ADD SAVINGS ACCOUNT \n2. ADD CURRENT ACCOUNT \n3. VIEW ALL TYPES OF ACCOUNTS \n4. VIEW SAVINGS/CURRENT ACCOUNT \n5. WITHDRAWAL/DEPOSIT \n\n");
			System.out.print("Enter your choice: ");
			choice = input.nextByte();
			switch(choice)
			{
				case 1:		//for adding savings account
				if(i>s1-1)	//here whenever savings account is created i is incremented so i can go upto only (s1-1)
				{
					System.out.println("You can't add more savings account.");
				}
				else
				{
					no++;
					System.out.println("Your account no is: " + no);
					for(k=0; ; k++)	//to check condition of balance since balance has to be kept at a min of 1000rs for savings account
					{
						System.out.print("How much balance do you want to keep? ");
						bal = input.nextDouble();
						if(bal>=1000)
						{
							break;	
						}
						else
						{
							System.out.println("Balance has to be at least 1000rs for opening savings account.");
						}
					}
					s[i] = new Savings(no, bal, roi);	//creating object of Savings class by calling it's parametrised Constructor
					for(k=0; ; k++)	//to check condition of rate of interest which has to be greater than 0
					{
						System.out.print("Enter rate of interest: ");
						roi = input.nextFloat();
						if(s[i].set_rate(roi))
						{
							break;
						}
						else
						{
							System.out.println("Rate of interest must be greater than 0.");
						}
					}
					for(k=0; ; k++)	//to check condition of month which has to be greater than 0
					{
						System.out.print("Enter the tenure of your savings(in months): ");
						monthX = input.nextInt();
						if(s[i].interestCalculate(monthX))
						{
							break;
						}
						else
						{
							System.out.println("Month should be at least 1.");
						}
					}
					System.out.println("Account sccessfully created.\n");
					i++;					
				}
				break;
				case 2:		//for adding current account
				if(j>c1-1)	//th  check condition that current account does not exceed the size that is allocated at runtine in the variable c1
				{
					System.out.println("You can't add more current account.\n");
				}
				else
				{
					no++;	//automatic account no generator
					System.out.println("Your acc no is: " + no);
					for(k=0; ; k++)	//check balance is not less than 0 for current account
					{
						System.out.print("How much balance do you want to keep? ");
						bal = input.nextDouble();
						if(bal>0)
						{
							break;	
						}
						else
						{
							System.out.println("Balance must be greater than 0 for current account.");
						}
					}
					c[j] = new Current(no, bal, overdraft);	//creating object of current account
					for(k=0; ; k++)		//to check overdraft limit is between "500" and "1Cr"rs for current account
					{
						System.out.print("Enter overdraft limit: ");
						overdraft = input.nextDouble();
						if(c[i].set_overdraft(overdraft))
						{
							break;
						}
						else
						{
							System.out.println("Overdraft limit must be at least 500rs.");
						}
					}
					System.out.println("Account sccessfully created.\n");
					j++;					
				}
				break;
				case 3:		//to view all types of account
				for(k=0; k<i; k++)
				{
					System.out.println("Savings: ");
					s[k].display();
					System.out.println("Rate of interest : " + s[k].get_rate());
					System.out.println("Tenure: " + s[k].get_month());
					System.out.println("Total Interest: " + s[k].get_interest());
					System.out.print("\n");
				}
				for(k=0; k<j; k++)
				{
					System.out.println("Current: ");
					c[k].display();
					System.out.println("Overdraft limit : " + c[k].get_overdraft());
					System.out.print("\n");
				}
				break;
				case 4:	//to view specific types of account
				System.out.print("1) SAVINGS \n2) CURRENT \n");
				System.out.print("Enter your choice: ");
				o = input.nextByte();
				if(o==1)	//implementation of dynamic polymorphism
				{
					a=s;		//since 1 is for savings account i have referred reference of Account class a to object of Savings class s
					l=i;		//how many accounts have been created has been presented by i
				}				
				else
				{
					a=c;		//since 2 is for current account i have referred reference of Account class a to object of Current class c
					l=j;
				}
				for(k=0; k<l; k++)
				{
					a[k].display();	//to display particular type of account
					
					//since below function prototypes(get_rate(), get_month(), get_interest(), get_overdraft()) are not declared in Account class therefore dynamic polymorphism cannot be implemented and so they have been separately printed for both current and savings class
					if(o==1)
					{	
						System.out.println("Rate of interest: " + s[k].get_rate());	
						System.out.println("Tenure: " + s[k].get_month());
						System.out.println("Total Interest: " + s[k].get_interest());
					}
					else
					{
						System.out.println("Overdraft Limit: " + c[k].get_overdraft());
					}
					System.out.print("\n");
				}
				break;
				case 5:	//for withdrawal and deposit
				System.out.println("From which type of account do you want to withdraw or deposit? ");
				System.out.println("1) SAVINGS \n2) CURRENT \n");
				System.out.print("Enter your choice: ");
				x1 = input.nextByte();
				if(x1==1)	//implementation of dynamic polymorphism(same as above which has been used in displaying accounts)
				{
					a=s;		//referring reference of class Account to object of Savings class
					l=i;
				}
				else
				{
					a=c;		//referring reference of class Account to object of Current class
					l=j;
				}
				System.out.println("1) WITHDRAW \n2) DEPOSIT");
				System.out.print("Enter your choice: ");
				x1 = input.nextByte();
				switch(x1)	//switch case for withdrawal and deposit
				{
					case 1:		//withdrawal
					System.out.print("Enter your acc no: ");
					accountNo = input.nextDouble();
					System.out.print("How much money do you want to withdraw? ");
					money = input.nextDouble();
					for(k=0; k<l; k++)
					{
						if(a[k].withdraw(money, accountNo))
						{
							System.out.println("Successfull...");
							flag = 0;
							break;
						}
					}
					if(flag==1)
					{
						System.out.println("Unsuccessful...");
					}
					break;
					case 2:		//deposit
					System.out.print("Enter your acc no: ");
					accountNo = input.nextDouble();
					System.out.print("How much money do you want to deposit? ");
					money = input.nextDouble();
					for(k=0; k<l; k++)
					{
						if(a[k].deposit(money, accountNo))
						{
							System.out.println("Successfull...");
							flag = 0;
							break;
						}
					}
					if(flag==1)
					{
						System.out.println("Unsuccessful...");
					}
					break; 
				}
				break;
			}
		}while(choice!=0);
	}
}