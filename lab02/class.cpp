#include<iostream>
#include<cstdlib>

using namespace std;

class Customer			//defining class of Customer
{
    public:
    char name[10];
    int cid;
    char gender;
    char phone[10];
    char address[100];
    void input();
    Customer();
	~Customer();
};

class Product			//defining class of Product
{
    public:
    char name[10];
    long int rate;
    long int qty;
    void pinput();
};

void Customer :: input()	//defining member function input
{
    cout<<"Customer Name : ";
    cin>>name;
    cout<<"Customer ID : ";
    cin>>cid;
    cout<<"Gender(m/f)? ";
    cin>>gender;
    cout<<"Phone no : ";
    cin>>phone;
    cout<<"Address : ";
    cin>>address;
}

Customer :: Customer()		//constructor
{
	name[0] = NULL;
	cid = 0;
	gender = NULL;
	phone[0] = NULL;
	address[0] = NULL;
}

void Product :: pinput()	//defining member function pinput for input of products
{
    int in, in1;
    cout<<"1) Footwears"<<endl;
    cout<<"2) Electronics"<<endl;
    cout<<"3) Fruits"<<endl;
    cout<<"\n";
    cout<<"Which category do you want to go? ";
    cin>>in;
    switch(in)			//switch case to go to your desired category
    {
        case 1:			//first case for category of footwears
        cout<<"1) Nike   -- 5000rs"<<endl;
        cout<<"2) Puma   -- 4000rs"<<endl;
        cout<<"3) Reebok -- 10000rs"<<endl;
        cout<<"4) China  -- 1000rs"<<endl;
        cout<<"\nWhich product do you want? ";
        cin>>in1;
        cout<<"\n";
        switch(in1)		//nested switch case to select your favorite product
        {
            case 1:
            cout<<"Product : Nike"<<endl;
            rate = 5000;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 2:
            cout<<"Product : Puma"<<endl;
            rate = 4000;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 3:
            cout<<"Product : Reebok"<<endl;
            rate = 10000;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 4:
            cout<<"Product : China"<<endl;
            rate = 1000;
            cout<<"Qty : ";
            cin>>qty;
            break;
        }
        break;

        case 2:				//second case for category of electronics
        cout<<"1) Dell   -- 50000rs"<<endl;
        cout<<"2) Lenovo -- 40000rs"<<endl;
        cout<<"3) Hp     -- 100000rs"<<endl;
        cout<<"4) ASUS   -- 10000rs"<<endl;
        cout<<"\nWhich product do you want? ";
        cin>>in1;
        cout<<"\n";
        switch(in1)
        {
            case 1:
            cout<<"Product : Dell"<<endl;
            rate = 50000;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 2:
            cout<<"Product : Lenovo"<<endl;
            rate = 40000;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 3:
            cout<<"Product : Hp"<<endl;
            rate = 100000;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 4:
            cout<<"Product : ASUS"<<endl;
            rate = 10000;
            cout<<"Qty : ";
            cin>>qty;
            break;
        }
        break;

        case 3:				//third case for category of fruits
        cout<<"1) Apple  -- 500rs"<<endl;
        cout<<"2) Orange -- 400rs"<<endl;
        cout<<"3) Mango  -- 1000rs"<<endl;
        cout<<"4) Banana -- 100rs"<<endl;
        cout<<"\nWhich product do you want? ";
        cin>>in1;
        cout<<"\n";
        switch(in1)
        {
            case 1:
            cout<<"Product : Apple"<<endl;
            rate = 500;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 2:
            cout<<"Product :Orange "<<endl;
            rate = 400;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 3:
            cout<<"Product : Mango"<<endl;
            rate = 1000;
            cout<<"Qty : ";
            cin>>qty;
            break;
            case 4:
            cout<<"Product : Banana"<<endl;
            rate = 100;
            cout<<"Qty : ";
            cin>>qty;
            break;
        }
        break;
    }
}

Customer :: ~Customer()            //destructor will be called 10 times since we are defining 10 elements of class Customer
{
	cout<<"Destructor is being called\n";
}



int main()			//main function

{
    int i, j, k=0, cid2, cno, nop;
    long int sum=0, amt;
    Customer c[10];				//automatic constructor is being called 10times on defining c[10]
    Product p[10];
    cout<<"Enter no of customers : ";		//no of customers to be entered
    cin>>cno;
    cout<<"\n";
    for(i=0; i<cno; i++)
    {
        c[i].input();
        cout<<"\n";
    }
    cout<<"Enter customer id to be searched : ";
    cin>>cid2;
    cout<<"\n";
    do					//loop for selecting your products
    {
        if(cid2==c[k].cid)		//matches the customer id with entered customer id
        {
            cout<<"Customer Name : "<<c[k].name<<endl;
            cout<<"\n";
            cout<<"How many products do you want?";
            cin>>nop;
            cout<<"\n";
            for(j=0; j<nop; j++)	//loop to enter products
            {
                p[j].pinput();
                cout<<"\n";
                sum+=p[j].qty*p[j].rate;
            }
        }
        k++;
    }while(cid2==c[k].cid);
    cout<<"\n";
    cout<<"You have to pay "<<sum<<"Rs"<<endl;
    cout<<"\n";

    do				//loop for checking amt to check if you have paid amt >= to the original one
    {
        cout<<"How much amt are you giving? ";
        cin>>amt;
        cout<<"\n";
        if(amt<sum)
        {
            cout<<"INVALID Amt"<<endl;
            cout<<"Amt >= "<<sum<<"Rs"<<endl<<endl;
        }
        else
        {
            cout<<"                     ***********  BILL  ***********"<<endl<<endl;	//generate bill
            cout<<"             Customer Name                : "<<c[k-1].name<<endl;
            cout<<"             Customer ID                  : "<<c[k-1].cid<<endl;
            cout<<"             Gender                       : "<<c[k-1].gender<<endl;
            cout<<"             Address                      : "<<c[k-1].address<<endl;
            cout<<"             Total Amt to be paid         : "<<sum<<"Rs"<<endl;
            cout<<"             You have paid                : "<<amt<<"Rs"<<endl;
            cout<<"             Change                       : "<<amt-sum<<"Rs"<<endl<<endl;
            cout<<" ****************  Thank you for shopping with us   ****************"<<endl<<endl;
        }
    }while(amt<sum);
    return 0;
}
