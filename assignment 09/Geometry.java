import java.util.*;
import java.io.*;

abstract class Shape implements Serializable	//abstract class Shape
{
	protected double area;
	
	Shape()		//Constructor
	{
		area = 0;
	}
}
abstract class TwoDimension extends Shape implements Serializable	//child class of Shape
{
	TwoDimension()
	{
		super();
	}
	abstract public double findArea();
}

abstract class ThreeDimension extends Shape implements Serializable//child class of Shape
{
	protected double volume;
	ThreeDimension()	//Constructor
	{
		super();	//calling super class constructor
	}
	abstract public double findSurfaceArea();//declared only definition because it's implementation is different in both 2D and 3D and kept abstract so that the child class has to declare it's body wothout any choice
	abstract public double findVolume();
}

class Rectangle extends TwoDimension implements Serializable	//child class of TwoDimension
{
	private double length;
	private double breadth;
	private float [][]vertice = new float[4][2];	//two dimensional matrix for vertices
	Rectangle()	//Constructor
	{
		super();	//calling super class constructor
		length = 0;
		breadth = 0;
	}
	public void setVertices(float vertice1, float vertice2, float vertice3, float vertice4, float vertice5, float vertice6, float vertice7, float vertice8)	//setting vertices
	{
		vertice[0][0] = vertice1;
		vertice[0][1] = vertice2;
		vertice[1][0] = vertice3;
		vertice[1][1] = vertice4;
		vertice[2][0] = vertice5;
		vertice[2][1] = vertice6;
		vertice[3][0] = vertice7;
		vertice[3][1] = vertice8;
	}
	public double getLength()	//getting length by distance formulae
	{
		length = Math.sqrt(Math.pow((vertice[0][0] - vertice[2][0]), 2) + Math.pow((vertice[0][1] - vertice[2][1]), 2));
		return length;
	}
	public double getBreadth()	//getting breadth by distance formulae
	{
		breadth = Math.sqrt(Math.pow((vertice[0][0] - vertice[1][0]), 2) + Math.pow((vertice[0][1] - vertice[1][1]), 2));
		return breadth;
	}
	public double findArea()	//getting area of rectangle by formula "length*breadth"
	{
		return length*breadth;
	}
	public void display()	//to display as per our format
	{
		System.out.println("Length: " + length);
		System.out.println("Breadth: " + breadth);
		System.out.println("Area: " + area);
	}
	public void writeRectangle(RandomAccessFile ioFile) throws IOException	//to write data to the file and it is declared here in the class because to access private members of the class
	{
		try	//try catch block to write data to a file
		{
			ioFile.writeDouble(getLength());
			ioFile.writeDouble(getBreadth());
			ioFile.writeDouble(findArea());
		}
		catch(IOException e)	//rethrowing exception so that it can be handled in main function
		{
			throw e;
		}
	}
	public void readRectangle(RandomAccessFile ioFile) throws IOException	//to read data from the file and it is also declared to access private members of this class
	{
		try	//try catch block for writing data to a file
		{
			length = ioFile.readDouble();
			breadth = ioFile.readDouble();
			area = ioFile.readDouble();
		}
		catch(IOException e)	//rethrowing exception so that it can be handled in main function
		{
			throw e;
		}
	}
}

class Circle extends TwoDimension implements Serializable //child class of TwoDimension
{
	private float []center = new float[2];	//array of center
	private float radius;
	Circle()	//constructor
	{
		super();
		center[0] = 0;
		center[1] = 0;
		radius = 0;
	}
	public void setCenter(float center1, float center2)	//setting centers of Circle
	{
		center[0] = center1;
		center[1] = center2;
	}
	public void setRadius(float radiusX)	//setting radius
	{
		radius = radiusX;
	}
	public double findArea()	//finding area of circle by the formulae "pi*r*r"
	{
		return (22 * radius * radius)/7;
	}
	public void display()	//to display as per our format
	{
		System.out.println("Radius: " + radius);
		System.out.printf("Center: (%f, %f)", center[0], center[1]);
		System.out.println("\nArea: " + area);
	}
	public void writeCircle(RandomAccessFile ioFile) throws IOException		//to write data to a file
	{
		try		//try catch block to write data
		{
			ioFile.writeFloat(radius);
			ioFile.writeFloat(center[0]);
			ioFile.writeFloat(center[1]);
			ioFile.writeDouble(findArea());
		}
		catch(IOException e)	//exception rethrown
		{
			throw e;
		}
	}
	public void readCircle(RandomAccessFile ioFile) throws IOException	//reading data from the file
	{
		try	//try catch block for reading data
		{
			radius = ioFile.readFloat();
			center[0] = ioFile.readFloat();
			center[1] = ioFile.readFloat();
			area = ioFile.readDouble();
		}
		catch(IOException e)	//exception rethrown
		{
			throw e;
		}
	}
}

class Cuboid extends ThreeDimension implements Serializable	//child class of ThreeDimension
{
	private float[][]verticC = new float[4][2];	//two dimensional array for vertices of Cuboid
	private double length;
	private double breadth;
	private double height;
	Cuboid()		//Constructor
	{
		super();	//calling super class constructor
		length = 0;
		breadth = 0;
		height = 0;
		volume = 0;
	}
	public void setVertices(float vertice1C, float vertice2C, float vertice3C, float vertice4C, float vertice5C, float vertice6C, float vertice7C, float vertice8C)	//setting vertices of cuboid
	{
		verticC[0][0] = vertice1C;
		verticC[0][1] = vertice2C;
		verticC[1][0] = vertice3C;
		verticC[1][1] = vertice4C;
		verticC[2][0] = vertice5C;
		verticC[2][1] = vertice6C;
		verticC[3][0] = vertice7C;
		verticC[3][1] = vertice8C;
	}
	public double getLength()	//returning length 
	{
		length = Math.sqrt(Math.pow((verticC[0][0] - verticC[2][0]), 2) + Math.pow((verticC[0][1] - verticC[2][1]), 2));
		return length;
	}
	public double getBreadth()	//returning breadth
	{
		breadth = Math.sqrt(Math.pow((verticC[0][0] - verticC[1][0]), 2) + Math.pow((verticC[0][1] - verticC[1][1]), 2));
		return breadth;
	}
	public double getHeight()	//returning height
	{
		height = Math.sqrt(Math.pow((verticC[2][0] - verticC[3][0]), 2) + Math.pow((verticC[2][1] - verticC[3][1]), 2));
		return height;
	}
	public double findSurfaceArea()	//returning surface area by the formula "2(lb + bh + lh)"
	{
		return 2*(length*breadth + breadth*height + length*height);
	}
	public double findVolume()		//returning volume by the formula "lbh"
	{
		return length*breadth*height;
	}
	public void display()		//displaying as per our format
	{
		System.out.println("Length: " + length);
		System.out.println("Breadth: " + breadth);
		System.out.println("Height: " + height);
		System.out.println("Area: " + area);
		System.out.println("Volume: " + volume);
	}
	public void writeCuboid(RandomAccessFile ioFile) throws IOException	//writing data of cuboid to the file using RandomAccessFile function
	{
		try	//try catch block to write data to the file
		{
			ioFile.writeDouble(getLength());
			ioFile.writeDouble(getBreadth());
			ioFile.writeDouble(getHeight());
			ioFile.writeDouble(findSurfaceArea());
			ioFile.writeDouble(findVolume());
		}
		catch(IOException e)	//exception rethrown
		{
			throw e;
		}
	}
	public void readCuboid(RandomAccessFile ioFile) throws IOException	//reading data from the file
	{
		try	//try catch block for reading data from the file
		{
			length = ioFile.readDouble();
			breadth = ioFile.readDouble();
			height = ioFile.readDouble();
			area = ioFile.readDouble();
			volume = ioFile.readDouble();
		}
		catch(IOException e)	//exception rethrown
		{
			throw e;
		}
	}
}

class Sphere extends ThreeDimension implements Serializable	//child class of ThreeDimension
{
	private float []center = new float[3];	//array for storing centers of sphere
	private float radius;
	Sphere()	//Constructor
	{
		super();
		center[0] = 0;
		center[1] = 0;
		center[2] = 0;
		radius = 0;
		volume = 0;
	}
	public void setCenter(float center1, float center2, float center3)	//setting centers of sphere
	{
		center[0] = center1;
		center[1] = center2;
		center[2] = center3;
	}
	public void setRadius(float radiusX)	//setting radius
	{
		radius = radiusX;
	}
	public float getRadius()	//returning radius
	{
		return radius;
	}
	public double findSurfaceArea()	//returning surface area by the formula "4*pi*r*r"
	{
		return (4*22*radius*radius)/7;
	}
	public double findVolume()	//returning volume by the formula "4/3*pi*r*r*r"
	{
		return (4*22*radius*radius*radius)/(7*3);
	}
	public void display()	//displaying as per our format
	{
		System.out.println("Radius: " + radius);
		System.out.printf("Center: (%f, %f, %f)", center[0], center[1], center[2]);
		System.out.println("\nArea: " + area);
		System.out.println("Volume: " + volume);
	}
	public void writeSphere(RandomAccessFile ioFile) throws IOException	//writing data to the file using RandomAccessFile function
	{
		try
		{
			ioFile.writeFloat(radius);
			ioFile.writeFloat(center[0]);
			ioFile.writeFloat(center[1]);
			ioFile.writeFloat(center[2]);
			ioFile.writeDouble(findSurfaceArea());
			ioFile.writeDouble(findVolume());
		}
		catch(IOException e)//exception rethrown
		{
			throw e;
		}
	}
	public void readSphere(RandomAccessFile ioFile) throws IOException	//reading data from the file
	{
		try	//try catch block for fetching data from the file
		{
			radius = ioFile.readFloat();
			center[0] = ioFile.readFloat();
			center[1] = ioFile.readFloat();
			center[2] = ioFile.readFloat();
			area = ioFile.readDouble();
			volume = ioFile.readDouble();
		}
		catch(IOException e)	//exception rethrown
		{
			throw e;
		}
	}
}



class Geometry		//main class
{
	public static void main(String []args)		//main function
	{
		Scanner input = new Scanner(System.in);
		byte choice;
		int position, i;
		float vertice1Y, vertice2Y, vertice3Y, vertice4Y, vertice5Y, vertice6Y, vertice7Y, vertice8Y, radiusX, xCoordinate, yCoordinate, zCoordinate;
		String colorX;
		do	//do while loop so that menu driven program runs until we press 0
		{
			System.out.print("\n0. Exit \n1. Append Rectangle \n2. Append Circle \n3. Append Cuboid \n4. Append Sphere \n5. View all rectangles \n6. View all circles \n7. View all cuboids \n8. View all Spheres \n9. Search specific rectangle \n10. Search specific Circle \n11. Search specific Cuboid \n12. Search specific Sphere \n\n");
			choice = input.nextByte();
			switch(choice)
			{
				case 1:	//Append Rectangle
				try	//try catch block to take input
				{
					RandomAccessFile rFile = new RandomAccessFile("Rectangles.dat", "rw");	//opening and creating file through RandomAccessFile function
					Rectangle r = new Rectangle();	//creating object of Rectangle class
					//taking input of vertices
					System.out.println("Topmost left vertices: ");
					System.out.print("Vertice 1: ");
					vertice1Y = input.nextFloat();
					System.out.print("Vertice 2: ");
					vertice2Y = input.nextFloat();
					System.out.println("Topmost right vertices: ");
					System.out.print("Vertice 1: ");
					vertice3Y = input.nextFloat();
					System.out.print("Vertice 2: ");
					vertice4Y = input.nextFloat();
					System.out.println("Bottommost left vertices: ");
					System.out.print("Vertice 1: ");
					vertice5Y = input.nextFloat();
					System.out.print("Vertice 2: ");
					vertice6Y = input.nextFloat();
					System.out.println("Bottommost right vertices: ");
					System.out.print("Vertice 1: ");
					vertice7Y = input.nextFloat();
					System.out.print("Vertice 2: ");
					vertice8Y = input.nextFloat();
					r.setVertices(vertice1Y, vertice2Y, vertice3Y, vertice4Y, vertice5Y, vertice6Y, vertice7Y, vertice8Y);
					rFile.seek(rFile.length());//seek function would set the pointer to the last line of the file and starts appending the new object from there
					r.writeRectangle(rFile);//calling writeRectangle function of Rectangle class
				}
				catch(IOException e)	
				{
					System.out.println("Error: " + e);
				}
				break;
				case 2:	//Append Circle
				try
				{
					RandomAccessFile rFile = new RandomAccessFile("Circles.dat", "rw");//opening and creating file through RandomAccessFile function
					Circle c = new Circle();//making object of circle
					//inputs of circle object
					System.out.println("Enter the centers of the circle: ");
					System.out.print("X co-ordinate: ");
					xCoordinate = input.nextFloat();
					System.out.print("Y co-ordinate: ");
					yCoordinate = input.nextFloat();
					System.out.print("Enter the radius of the circle: ");
					radiusX = input.nextFloat();
					c.setCenter(xCoordinate, yCoordinate);
					c.setRadius(radiusX);
					rFile.seek(rFile.length());
					c.writeCircle(rFile);
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 3:	//Appending Cuboid data
				try
				{
					Cuboid cube = new Cuboid();
					RandomAccessFile rFile = new RandomAccessFile("Cuboids.dat", "rw");
					System.out.println("Bottom left most vertices: ");
					System.out.print("Verice1: ");
					vertice1Y = input.nextFloat();
					System.out.print("Verice2: ");
					vertice2Y = input.nextFloat();
					System.out.println("Bottom right most vertices: ");
					System.out.print("Verice1: ");
					vertice3Y = input.nextFloat();
					System.out.print("Verice2: ");
					vertice4Y = input.nextFloat();
					System.out.println("Upper vertice to bottom left most vertice: ");
					System.out.print("Verice1: ");
					vertice5Y = input.nextFloat();
					System.out.print("Verice2: ");
					vertice6Y = input.nextFloat();
					System.out.println("Vertice in z plane: ");
					System.out.print("Verice1: ");
					vertice7Y = input.nextFloat();
					System.out.print("Verice2: ");
					vertice8Y = input.nextFloat();
					cube.setVertices(vertice1Y, vertice2Y, vertice3Y, vertice4Y, vertice5Y, vertice6Y, vertice7Y, vertice8Y);
					rFile.seek(rFile.length());
					cube.writeCuboid(rFile);
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 4:		//Appending Sphere data
				try
				{
					RandomAccessFile rFile = new RandomAccessFile("Spheres.dat", "rw");
					Sphere s = new Sphere();
					System.out.println("Enter the centers of sphere: ");
					System.out.print("X co-ordinate: ");
					xCoordinate = input.nextFloat();
					System.out.print("Y co-ordinate: ");
					yCoordinate = input.nextFloat();
					System.out.print("Z co-ordinate: ");
					zCoordinate = input.nextFloat();
					System.out.print("Enter the radius of the sphere: ");
					radiusX = input.nextFloat();
					s.setCenter(xCoordinate, yCoordinate, zCoordinate);
					s.setRadius(radiusX);
					rFile.seek(rFile.length());
					s.writeSphere(rFile);
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 5:	//To view all Rectangles data
				try	//try catch block to view all data of rectangles
				{
					i=0;
					RandomAccessFile rFile = new RandomAccessFile("Rectangles.dat", "rw");
					Rectangle r = new Rectangle();
					rFile.seek(0);//this would set file pointer to the start of the file
					while(rFile.getFilePointer() < rFile.length())//loop would continue till the end of the file
					{
						System.out.println("Rectangle " + (i+1) + ": ");
						//reading and displaying data from file
						r.readRectangle(rFile);
						r.display();
						System.out.print("\n");
						i++;
					}
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 6:	//to view all Circles data
				{
					try
					{
						i=0;
						RandomAccessFile rFile = new RandomAccessFile("Circles.dat", "rw");
						Circle c = new Circle();
						rFile.seek(0);
						while(rFile.getFilePointer() < rFile.length())
						{
							System.out.println("Circle " + (i+1) + ": ");
							c.readCircle(rFile);
							c.display();
							System.out.print("\n");
							i++;
						}
					}
					catch(IOException e)
					{
						System.out.println("Error: " + e);
					}
				}
				break;
				case 7://To view all Cuboid data
				try
				{
					i=0;
					RandomAccessFile rFile = new RandomAccessFile("Cuboids.dat", "rw");
					Cuboid cube = new Cuboid();
					rFile.seek(0);
					while(rFile.getFilePointer() < rFile.length())
					{
						System.out.println("Cuboid " + (i+1) + ": ");
						cube.readCuboid(rFile);
						cube.display();
						System.out.print("\n");
						i++;
					}
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 8://to view all spheres data
				try
				{
					i=0;
					RandomAccessFile rFile = new RandomAccessFile("Spheres.dat", "rw");
					Sphere s = new Sphere();
					rFile.seek(0);
					while(rFile.getFilePointer() < rFile.length())
					{
						System.out.println("Sphere " + (i+1) + ": ");
						s.readSphere(rFile);
						s.display();
						System.out.print("\n");
						i++;
					}
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 9:	//to view particular rectangle
				try	//try catch block to view particular records
				{
					System.out.print("Enter position of rectangle: ");	//first we have to give position of rectangle which we want to view
					position = input.nextInt();
					RandomAccessFile rFile = new RandomAccessFile("Rectangles.dat", "rw");
					Rectangle r = new Rectangle();
					rFile.seek((position-1)*24);//the pointer refers to the object which is one lesser than that so that the pointer gets start with the data which we want to fetch
					//here i have also multiplied by 24 since it is the size of the object and we want to display whole data of 24 bytes of object therefore we have multiplied by 24
					r.readRectangle(rFile);
					r.display();	//displaying
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 10:	//to view specific Circle
				try
				{
					System.out.print("Enter position of Circle: ");
					position = input.nextInt();
					RandomAccessFile rFile = new RandomAccessFile("Circles.dat", "rw");
					Circle c = new Circle();
					rFile.seek((position-1)*20);//here size is set to 20 since object of Circle is of 20 bytes
					c.readCircle(rFile);
					c.display();
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 11:	//to view specific cuboid
				try
				{
					System.out.print("Enter position of Cuboid: ");
					position = input.nextInt();
					RandomAccessFile rFile = new RandomAccessFile("Cuboids.dat", "rw");
					Cuboid cube = new Cuboid();
					rFile.seek((position-1)*40);	//size of Cuboid object is of 40 bytes
					cube.readCuboid(rFile);
					cube.display();
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
				case 12:	//to view specific sphere
				try
				{
					System.out.print("Enter position of Sphere: ");
					position = input.nextInt();
					RandomAccessFile rFile = new RandomAccessFile("Spheres.dat", "rw");
					Sphere s = new Sphere();
					rFile.seek((position-1)*20);	//size of sphere object is of 20 bytes
					s.readSphere(rFile);
					s.display();
				}
				catch(IOException e)
				{
					System.out.println("Error: " + e);
				}
				break;
			}
		}while(choice!=0);
	}
}