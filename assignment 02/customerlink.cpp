#include<iostream>
#include<cstdlib>
#include<string.h>

using namespace std;

namespace ns1
{
	class Customer				//customer class
	{
		public:
		char name[10];
		int cid;
		char gender;
		char phoneNo[10];
		char address[50];
		char pname[10];
		long int rate;
		int qty;
		long int sum;
		Customer *next;
		Customer *start=NULL;		//initiating three pointers to null
		Customer *curr=NULL;
		Customer *temp=NULL;
		void getData();
		void putData();
		void showData();
		void deleteData();
		Customer();
		~Customer();
	};
}

using namespace ns1;				//using namespace in which customer class is stored

Customer :: Customer()				//Constructor
{
	name[0]=NULL;
	cid=0;
	gender=NULL;
	phoneNo[0]=NULL;
	address[0]=NULL;
	pname[0]=NULL;
	rate=0;
	qty=0;
	sum=0;
}

void Customer :: getData()
{
	int i, no;
	char ch;
	do					//loop to enter details
	{
		temp=new Customer[1];
		cout<<"Customer Name : ";
		cin>>temp->name;
		cout<<"Customer ID : ";
		cin>>temp->cid;
		cout<<"Gender : ";
		cin>>temp->gender;
		cout<<"Phone No : ";
		cin>>temp->phoneNo;
		cout<<"Address : ";
		cin>>temp->address;
		cout<<"How many products do you want? ";
		cin>>no;
		for(i=0, temp->sum=0; i<no; i++)
		{
			cout<<"Product Name : ";
			cin>>temp->pname;
			cout<<"Product Rate : ";
			cin>>temp->rate;
			cout<<"Qty : ";
			cin>>temp->qty;
			temp->sum+=(temp->rate)*(temp->qty);
		}
		cout<<"\n";
		if(start==NULL)				//making all three pointers start, curr & temp equal to the base address temp
		{
			start=curr=temp;
		}
		else					//loop to link nodes through their addresses
		{
			curr->next=temp;
			curr=temp;
		}
		temp->next=NULL;
		cout<<"Do you want to add more data?(y/n) ";
		cin>>ch;
	}while(ch!='n');
}

void Customer :: showData()			//function to show details of customers unlike bill
{
	Customer *tmp;				//taking the base address and generating all records through that address
	for(tmp=start; tmp!=NULL; tmp=tmp->next)
	{
		cout<<"   Customer Name 		: "<<tmp->name<<endl;
		cout<<"   Customer ID 			: "<<tmp->cid<<endl;
		cout<<"   Gender 			: "<<tmp->gender<<endl;
		cout<<"   Phone No 			: "<<tmp->phoneNo<<endl;
		cout<<"   Address 			: "<<tmp->address<<endl;
		cout<<"\n";
	}
}

void Customer :: putData()			//function for generating bill
{
	int cid2;
	Customer *temp1;
	cout<<"Enter Customer ID : ";
	cin>>cid2;
	for(temp1=start; temp1->cid!=cid2; temp1=temp1->next)		//loop to check condition
	{}							//if condition satisfies than it generates bill of that customer
	cout<<"************************** BILL **************************\n\n";
	cout<<"   Customer Name 		: "<<temp1->name<<endl;
	cout<<"   Customer ID 			: "<<temp1->cid<<endl;
	cout<<"   Gender 			: "<<temp1->gender<<endl;
	cout<<"   Phone No 			: "<<temp1->phoneNo<<endl;
	cout<<"   Address 			: "<<temp1->address<<endl;
	cout<<"   Total Amt to be paid 	: "<<temp1->sum<<endl;
	cout<<"\n";
	cout<<"**************** Thank you for shopping with us ****************";
	cout<<"\n";
}

void Customer :: deleteData()			//function for deleting entry of customer
{
	int cid3;
	Customer *del=start;
	cout<<"Enter Customer ID : ";
	cin>>cid3;
	while(1)
	{
		if(del->cid==cid3)		//for deletion of head node
		{
			Customer *ptr1;
			ptr1=start;
			start=temp=curr=del->next;//making all three pointers equal to first address after the base address unlike 							    from base address
			free(ptr1);
			break;
		}
		else if((del->next)->cid == cid3)	//for deletion of node other than head node
		{
			Customer *ptr=del;
			ptr->next=del->next->next;
			free(ptr);
			break;
		}
		else
		{
			del=del->next;
		}
	};
}

Customer :: ~Customer()         //Destructor
{
	delete temp;
}

int main()

{
	int x;
	Customer c;				//creating object of customer class
	do
	{
		cout<<"1)ADD RECORDS \n2)GENERATE BILL \n3)DELETE RECORDS \n4)SHOW ALL RECORDS \n0)EXIT\n\n";
		cin>>x;
		cout<<"\n";
		switch(x)			//switch case for calling various functions
		{
			case 1:
			c.getData();		//calling getData function
			break;
			case 2:
			c.putData();		//calling putData function
			break;
			case 3:
			c.deleteData();		//calling deleteData function
			break;
			case 4:
			c.showData();		//calling showData function
			break;
			case 0:
			exit(0);
		}
	}while(1);
	return 0;
}



