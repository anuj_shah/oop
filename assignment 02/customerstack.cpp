#include<iostream>
#include<cstdlib>
#include<string.h>

namespace ns1
{
	class Customer					//Customer class
	{
	    public:
	    int stack=1;
	    char name[10];
	    int cid;
	    char gender;
	    char phoneNo[10];
	    char address[50];
	    char pname[10];
	    long int rate;
	    int qty;
	    long int sum[10];
	    int top=-1;
	    Customer *c1;
	    bool push(int);
	    void pop();
	    void getData();
	    Customer();
	    ~Customer();
	};
}

using namespace std;
using namespace ns1;

Customer :: Customer()					//Constructor
{
	name[0]=NULL;
	cid=0;
	gender=NULL;
	phoneNo[0]=NULL;
	address[0]=NULL;
	pname[0]=NULL;
	rate=0;
	qty=0;
	sum[0]=0;
}

void Customer :: getData()				//get input from user
{
    int no, i;
    cout<<"Customer Name : ";
    cin>>name;
    cout<<"Customer ID : ";
    cin>>cid;
    cout<<"Gender(m/f)? ";
    cin>>gender;
    cout<<"Phone no : ";
    cin>>phoneNo;
    cout<<"Address : ";
    cin>>address;
    cout<<"How many products do you want?";
    cin>>no;
    for(i=0, sum[i]=0; i<no; i++)			//loop for entering product details
    {
        cout<<"Product Name : ";
        cin>>pname;
        cout<<"Product rate : ";
        cin>>rate;
        cout<<"Qty : ";
        cin>>qty;
        sum[i] += qty*rate;
    }
}

bool Customer :: push(int size)				//member function push for pushing the data into the stack
{
	if(top>=size-1)					//checks size>top
	{
		return false;
	}
	else
	{
		if(stack==1)				//condition so that memory is allocated only one time
		{
			c1=new Customer[size];
		}
		top++;
		strcpy(c1[top].name, name);
		c1[top].cid=cid;
		strcpy(c1[top].address, address);
		c1[top].gender=gender;
		strcpy(c1[top].phoneNo, phoneNo);
		c1[top].sum[top]=sum[top];
		stack++;//incrementing stack so that it does not satisfy the condition of (stack==1) and so memory is allocated once
		return true;
        }
}

void Customer :: pop()				// member function for popping the pushed value into the stack.
{						//It would pop pushed value in the form of bill 
    if(top>-1)					//condition to check whether stack is empty or not
    {
        strcpy(name, c1[top].name);
        cid=c1[top].cid;
        gender=c1[top].gender;
        strcpy(address, c1[top].address);
        strcpy(phoneNo, c1[top].phoneNo);
        sum[top]=c1[top].sum[top];
        cout<<"*************************** BILL ***************************\n";        
        cout<<"\n";
        cout<<"Customer Name 		: "<<name<<endl;
        cout<<"Customer ID 		: "<<cid<<endl;
        cout<<"Customer Address 	: "<<address<<endl;
        cout<<"Customer phone No 	: "<<phoneNo<<endl;
        cout<<"Total Amt 		: "<<sum[top]<<endl;
        cout<<"\n";
        cout<<"*************  Thank you for shopping with us **************\n";
        cout<<"\n";
        top--;
    }
    else				//condition if stack is empty
    {
    	cout<<"Stack is empty\n\n";
    }
}

Customer :: ~Customer()			//Destructor
{
	delete c1;
}

int main()

{
	int x, no;
	Customer c1;
	cout<<"Enter no of customers : ";	//Whatever no of customers entered would corresponds to size of stack
	cin>>no;
	while(1)				//loop for push and pop that would run until we press 0
	{
		cout<<"1) Push\n2) Pop\n0) Exit\n\n";
		cin>>x;
		switch(x)
		{
		    case 1:
		    c1.getData();
		    c1.push(no);
		    cout<<"\n";
		    break;
		    case 2:
		    c1.pop();
		    cout<<"\n";    
		    break;
		    case 0:
		    exit(0);
		    break;
		}
   	};
    return 0;
}
