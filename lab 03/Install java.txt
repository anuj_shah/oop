=>Installing java in windows:
1) Go to the Manual download page.
2) Click on Windows Offline.
3) The File Download dialog box appears prompting you to run or save the download file 
Click Save to download the file to your local system.
Tip: Save the file to a known location on your computer, for example, to your desktop.
4) Close all applications including the browser.
5) Double-click on the saved file to start the installation process.
6) After the successful insatallation of java you have to go to my computer icon on desktop and right click on it and then select properties from the drop down list.
7) There you have to go to advanced system settings and after that you have to go to environment variables from it.
8) There you have to include a new user variable with variable name "Path" and it's value would be the path of the file in which there is bin file of java.
9) For example in my pc: Path: "C:\Program Files\Java\jdk1.6.0_25\bin".

=>Installing java in ubuntu
1) First you have to type javac in ubuntu terminal window.
2) Then it will show some list of java files.
3) Then you have to write the code "sudo apt-get install x" where x is name of version of java which you want to install.
4) If some errors occurs then you have to write "sudo apt-get install update".
5) Then you have to write "sudo apt-get intall upgrade".
6) Then you have to write javac.
7) And then continue with step 1.