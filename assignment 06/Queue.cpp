#include<iostream>

using namespace std;

class Queue		//Queue class
{	
	private:
	int size;
	double *value;		//array of value
	double value1;
	public:
	unsigned short int top1 = 0;
	unsigned short int top = 0;
	Queue(int);
	void operator +(double);		//operator + overloaded
	void operator --();			//operator -- overloaded
	friend ostream & operator <<(ostream &, Queue);	//operator << overloaded
};

Queue :: Queue(int size1)	//parametrised constructor that would allocate memory at runtime
{
	if(size1<0)			//if size is less than 0 then memory would not be allocated
	{
		cout<<"Error allocating memory."<<endl;
	}
	else					//if size>0 than memory would be allocated
	{
		size = size1;
		value = new double[size];
	}
}

void Queue :: operator +(double no)	//+ operator overloading
{
	if(top>size-1)		//queue would be full when top is greater than size-1
	{
		cout<<"Queue is full."<<endl;
	}
	else				
	{
		value1 = no;		//value1 would be the input of user and it would be stored in dimesion of point
		value[top] = value1;
		top++;
	}
}

void Queue :: operator --()		//-- operator overloading
{
	if(top1>top-1)		//top1 is greater than top-1 than queue would be empty
	{
		cout<<"Queue is empty."<<endl;
	}
	else
	{
		top--;			//top value would decrease as we dequeue the element from the queue
		cout<<"Dequeued value : "<<value[top1]<<endl;
		for(int i=0; i<top; i++)		//this loop would shift all the elements one step forward
		{
			value[i] = value[i+1];
		}
	}
}

ostream & operator <<(ostream &out, Queue q)		//<< operator overloaded		
{
	int i;
	for(i=0; i<q.top; i++)			//display all values of queue
	{
		out<<"Value : "<<q.value[i]<<endl;
	}
	return out;
}

int main()			//main function

{
	int sizeInput;
	unsigned short int x;
	double no;
	cout<<"Enter the size of the queue : ";			//to enter size of queue
	cin>>sizeInput;
	cout<<endl;
	Queue q1(sizeInput);			//create object of class Queue and allocate memory to it
	do
	{
		cout<<"0) TERMINATE \n1) ENQUEUE \n2) DEQUEUE\n3) DISPLAY ALL VALUES \n";
		cin>>x;
		cout<<endl;
		switch(x)
		{
			case 1:			//case 1 for queuing the element
			cout<<"Enter value : ";
			cin>>no;
			q1 + no;			//adding new element to the queue
			cout<<endl;
			break;
			case 2:			//for dequeuing
			--q1;
			cout<<endl;
			break;
			case 3:			//to display all values
			cout<<q1;
			cout<<endl;
			break;
		}
	}while(x!=0);
}



