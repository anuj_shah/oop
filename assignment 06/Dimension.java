import java.util.Scanner;

class Point				//class point
{
	private int size;
	private double []val;
	public int i=0;
	
	Point(Point p)		//copy constructor
	{
		size = p.size;		//copying size of p to size of newly created object of class Point
		val = p.val;		//copying size of value of p to newly created object of class Point 
		System.arraycopy(p.val, 0, val, 0, p.val.length); 	//copying elemnts of array of val to newly created object of class Point
	}
	
	public Point(int size1)		//parametrised constructor that would allocate dimensions to the points
	{
		size = size1;
		val = new double[size];
	}
	
	public void getData(double no)	//would take double as parameter and store in val of that point
	{
		val[i] = no;
		i++;
	}
	public void putData()		//to display any point in point form i.e. of the form (x1, x2, x3....)
	{
		for(int j=0; j<size; j++)
		{
			if(j==size-1)
			{
				System.out.printf("%f)", val[j]);
			}
			else if(j==0)
			{
				System.out.printf("(%f, ", val[j]);
			}
			else
			{
				System.out.printf("%f, ",val[j]);
			}
		}
	}
	
	public void equals(Point p)		//equality checker
	{
		int flag=1;			
		if(size==p.size)		//first it will check the dimension of two points
		{
			for(int k=0; k<size; k++)
			{
				if(val[k]!=p.val[k])	//if dimension is checked then every element of the point is checked with other element
				{
					flag = 0;
				}
			}
		}
		if(flag==0 || size!=p.size)		//if dimensions or elements of points are not equal then points would not be equal
		{
			System.out.println("Two points are not equal.");
		}
		else
		{
			System.out.println("Two points are equal.");
		}
	}
}

class Dimension		//Dimension class
{
	public static void main(String args[])		//main class
	{
		Scanner input = new Scanner(System.in);
		int x, no, size1, point1, point2;
		double no1;
		System.out.print("Enter the no of points you want to create: ");		//to enter the no of points anyone want to create
		no = input.nextInt();
		Point []po = new Point[no];			//creating array of objects
		for(int j=0; j<no; j++)
		{
			System.out.printf("Enter the no of dimensions for point %d ", j+1);		//enter the dimension for created points
			size1 = input.nextInt();
			po[j] = new Point(size1);				//allocating memory to the individual point
			for(int k=0; k<size1; k++)			//taking elements of point from user
			{
				System.out.printf("Enter value for position %d ", k+1);
				no1 = input.nextDouble();
				po[j].getData(no1);
			}
		}
		do
		{
			System.out.print("0) TERMINATE \n1) EQUALITY CHECKER \n2) SHOW ALL POINTS \n");
			x = input.nextInt();
			switch(x)
			{
				case 1:			//case 1 for equality checker
				System.out.println("Enter two points for checking equality ");
				System.out.print("Point 1 : ");
				point1 = input.nextInt();
				System.out.print("Point 2 : ");
				point2 = input.nextInt();
				if(point1<=no && point2<=no)	//to restrict the user to enter size of point less than no of points created
				{
					Point pp = new Point(po[point1-1]);		//using copy constructor to assign values of po point to pp point
					Point ppp = new Point(po[point2-1]);		//using copy constructor to assign values of po point to ppp point
					pp.equals(ppp);			//calling equals function
					System.out.print("\n");
				}
				else
				{
					System.out.println("Point doesn't exist.\n");
				}
				break;
				case 2:		//to display all points in point form
				for(int k=0; k<no; k++)
				{
					System.out.printf("Point %d : ", k+1);
					po[k].putData();
					System.out.print("\n");
				}
				break;
			}
		}while(x!=0);
	}
}