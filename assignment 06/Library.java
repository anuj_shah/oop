import java.util.Scanner;

class Media			//Media class
{
	protected String title;		//kept protected becuase they have been used in their child class
	protected int yearOfPublication;
	protected  double price;
	Media()		//constructor of Media class
	{
		title = null;
		yearOfPublication = 0;
		price = 0;
	}
}

class Book extends Media		//making child class of Media using keyword 'extends'
{
	Scanner input = new Scanner(System.in);
	private String author;
	private int no_of_pages;
	Book()		//constructor of child class i.e. Book
	{
		super();		//calling constructor of parent class i.e. Media using 'super' keyword
		author = null;
		no_of_pages = 0;
	}
	public void getData()		//for getting data from user
	{
		System.out.print("Enter title of the book : ");
		title = input.nextLine();
		System.out.print("Enter author of the book : ");
		author = input.nextLine();
		System.out.print("Enter year of publication : ");
		yearOfPublication = input.nextInt();
		System.out.print("Enter no of pages of the book : ");
		no_of_pages = input.nextInt();
		System.out.print("Enter price of the book : ");
		price = input.nextDouble();
	}
	public void putData()		//for displaying data which is entered by user
	{
		System.out.println("Title of the book : " + title);
		System.out.println("Author of the book : " + author);
		System.out.println("Title of the book : " + yearOfPublication);
		System.out.println("Title of the book : " + no_of_pages);
		System.out.println("Title of the book : " + price);
	}
}

class CD extends Media			//child class of Media i.e. CD
{
	Scanner input = new Scanner(System.in);
	private double sizeInMB;
	private int playtime;
	CD()				//constructor of child class i.e. CD
	{
		super();		//calling constructor of parent class i.e. Media using 'super' keyword
		sizeInMB = 0;
		playtime = 0;
	}
	public void getData()	//for getting input from user
	{
		System.out.print("Enter title of the CD : ");
		title = input.nextLine();
		System.out.print("Enter year of publication of the CD : ");
		yearOfPublication = input.nextInt();
		System.out.print("Enter size(in MB) of the CD : ");
		sizeInMB = input.nextDouble();
		System.out.print("Enter playtime of the CD : ");
		playtime = input.nextInt();
		System.out.print("Enter price of the CD : ");
		price = input.nextDouble();
	}
	public void putData()		//for displaying data which has been entered by user
	{
		System.out.println("Title of the CD : " + title);
		System.out.println("Year of publication of the CD : " + yearOfPublication);
		System.out.println("Size of the CD(in MB) : " + sizeInMB);
		System.out.println("Playtime of the CD : " + playtime);
		System.out.println("Price of the CD : " + price);
	}
}
	
class Library				//Library class
{
	public static void main(String args[])		//main class
	{
		int x;
		Scanner input = new Scanner(System.in);
		do
		{
			System.out.print("0) TERMINATE\n1) ENTER DATA OF CD AND DISPLAY \n2) ENTER DATA OF BOOK AND DISPLAY \n");
			x = input.nextInt();
			System.out.print("\n");	
			switch(x)
			{
				case 1:			//for getting and displaying data of CD child class
				CD c1 = new CD();
				c1.getData();
				System.out.print("\n");
				c1.putData();
				System.out.print("\n");
				break;
				case 2:			//for getting and displaying data of Book child class
				Book b1 = new Book();
				b1.getData();
				System.out.print("\n");
				b1.putData();
				System.out.print("\n");
			break;
			}
		}while(x!=0);
		
	}
}