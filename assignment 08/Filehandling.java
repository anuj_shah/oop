import java.io.*;
import java.util.Scanner;

class Filehandling	//main class
{
	public static void main(String args[])	//main function
	{	
		Scanner input = new Scanner(System.in);
		byte choice;
		String name, source, destination, oldName1, newName1;
		int i, count, f=0, count1;
		do
		{
			System.out.print("\n0. Terminate \n1. Count the no of characters \n2. Count the no of words \n3. Count the no of lines \n4. Copy the contents of one file to another \n5. Rename a file \n\n");
			System.out.print("Enter your choice: ");
			choice = input.nextByte();
			switch(choice)
			{
				case 1:		//for counting no of characters
				try	//try catch block for counting no of characters
				{
					//initialising count and count1 with 0
					count=0;	
					count1=0;
					System.out.print("Enter the name of file: ");
					name = input.next();
					FileInputStream fin = new FileInputStream(name);
					while((f=fin.read())!=-1) //till the end of the file no of characters with spacing and without spacing gets counted
					{
						if((char)f != '\n');	//if line does not occur then count gets incremented
							count++;
						if((char)f != '\n' && (char)f != ' ')	//if line does not occur and spaces also doesn't occur then count1 							gets incremented
							count1++;
					}
					System.out.println("Total no of characters(with spaces) are: " + count);
					System.out.println("Total no of characters(without spaces) are: " + count1);
				}
				catch(Exception e)
				{
					System.out.println("File not found.");
				}
				break;
				case 2:	//for counting no of words
				try	//try catch block for counting no of words
				{
					count = 1;	//initialising count with 1
					System.out.print("Enter the name of file: ");
					name = input.next();
					FileInputStream fin = new FileInputStream(name);
					while((f=fin.read())!=-1)	//till the end of the file no of words gets counted
					{
						if((char)f == ' ' || (char)f == '\n')	//if it encounters spaces or lines then count gets incremented
							count++;
					}
					System.out.println("Total no of words are: " + count);
				}
				catch(Exception e)
				{
					System.out.println("File not found.");
				}
				break;
				case 3:	//for counting no of lines
				try	//try catch block for counting no lines
				{
					count=1;	//initialising count with 1
					System.out.print("Enter the name of file: ");
					name = input.next();
					FileInputStream fin = new FileInputStream(name);
					while((f=fin.read())!=-1)	//counts no of lines till the end of the file
					{
						if((char)f == '\n')	//if it detects line then count gets incremented
							count++;
					}
					System.out.println("Total no of lines are: " + count);
				}
				catch(Exception e)
				{
					System.out.println("File not found.");
				}
				break;
				case 4:	//copying one file into another
				try
				{
					System.out.print("Enter source file: ");
					source = input.next();
					System.out.print("Enter destination file: ");
					destination = input.next();
					FileInputStream fin = new FileInputStream(source);
					FileOutputStream fout = new FileOutputStream(destination);
					//Input comes from source file one by one and gets written to destination file
					while((f=fin.read())!=-1)	
					{
						fout.write((byte)f);	//typecasting to byte to store in file
					}
					fin.close();
					fout.close();
					System.out.println("Copied Successfully.");
				}
				catch(Exception e)
				{
					System.out.println("File not found.");
					System.out.print("\n");
				}
				break;
				case 5:	//renaming file 
				System.out.print("Enter the file which you want to rename: ");
				oldName1 = input.next();
				File oldName = new File(oldName1);	//getting input of oldName
				System.out.print("Enter the name with which you want to rename: ");
				newName1 = input.next();		//getting input of newName
				File newName = new File(newName1);
				if(oldName.renameTo(newName))	//library function rename that has return type of boolean
				{
					System.out.println("Renamed successful.");
				}
				else
				{
					System.out.println("Rename unsuccessful.");
				}
				break;
			}
		}while(choice!=0);
	}
}
