import java.io.*;
import java.util.Scanner;

abstract class Account implements Serializable		//abstract parent class Account //implements Serializable so that we can write and read display from file as per our choice
{
	protected double accNo;		//kept protected so that it can be used in it's child class
	protected double balance;

	Account(double no, double bal)	//Constructor of Account class
	{
		accNo = no;
		balance = bal;
	}

	public boolean deposit(double money, double accountNo)	//deposit member function is common for savings and current therefore it has been kept in Account class
	{
		if(accountNo == accNo)	//first checks account no
		{
			if(money>=0)	//checks money(should be at least 0rs)
			{
				balance += money;
				return true;
			}
			return false;
		}
		return false;
	}
	abstract public boolean withdraw(double money, double accountNo);  //only prototype has been defined so that it can be implemented in dynamic polymorphism
	//kept abstract so that any user who would create other child classes of Account has to create this member function and declare it's method
}

class Savings extends Account implements Serializable		//child class of Account
{
	private double totInterest=0;
	private double interest;
	private float rateOfInterest;
	private int l;
	private int month;
	Savings(double no, double bal, float rate)	//Constructor of child class Savings
	{
		super(no, bal);	//calling Constructor of parent class Account using 'super' keyword
		rate = 3.5f;
	}

	public boolean interestCalculate(int mon)	//to calculate interest
	{
		if(mon>0)		//first checks condition on month
		{
			month = mon;
			for(l=1; l<=month; l++)		//loop to calculate interest once in each quarter which has been checked by (l%4==0)
			{
				if(l%3==0)
				{
					interest = ((balance)*(rateOfInterest)*(3))/1200;	//formula of interest (i = prn/1200) where time period is in months
					balance += interest;	//adding interest in balance
					totInterest+=interest;	//calculating total interest
				}
			}
			interest = ((balance)*(rateOfInterest)*(month%3))/1200;	//calculating interest for remaining amt of months
			balance += interest;
			totInterest += interest;
			return true;
		}
		return false;
	}

	public double get_interest() //returning total interest
	{
		return totInterest;
	}

	public int get_month()	//returning month
	{
		return month;
	}

	public boolean set_rate(float rate)	//setting rate of interest
	{
		if(rate>0)
		{
			rateOfInterest = rate;
			return true;
		}
		return false;
	}
	public float get_rate()		//returning rate of interest
	{
		return rateOfInterest;
	}
	public boolean withdraw(double money, double accountNo)	//withdraw member function for savings account
	{
		if(accountNo == accNo)		//first checks if account no are equal
		{
			if(money>=0 && money<=balance-500)	//since money has to be greater than 0 and there should be min balance of 500 in savings account
			{
				balance -= money;
				return true;
			}
			return false;
		}
		return false;
	}
	public void display()	//displaying data
	{
		System.out.println("Savings Account.");
		System.out.println("Account no: " + accNo);
		System.out.println("Balance: " + balance);
		System.out.println("Rate of interest: " + rateOfInterest);
		System.out.println("Tenure of savings(in months): " + month);
		System.out.println("Total interest: " + totInterest);
		System.out.print("\n");
	}
}

class Current extends Account implements Serializable	//second child class of parent class Account
{
	private double overdraftLimit;
	Current(double no, double bal, double overdraft)	//Constructor of child class Current
	{
		super(no, bal);		//calling parent class Constructor of Account
		overdraft = 500;
	}
	public boolean set_overdraft(double overdraft)		//setting overdraft limit
	{
		if(overdraft>=500 && overdraft<=10000000)	//overdraft limit has to be between "500"rs and "1Cr"rs.
		{
			overdraftLimit = overdraft;
			return true;
		}
		return false;
	}
	public double get_overdraft()		//returning overdraft
	{
		return overdraftLimit;
	}
	public boolean withdraw(double money, double accountNo)	//withdraw member function for Current account
	{
		if(accountNo == accNo)		//checks equality of account no
		{
			if(money>=0 && money<=balance+overdraftLimit)	//customer can withdraw money upto the negative balance beyond the overdraft
			{
				balance -= money;
				return true;
			}
			return false;
		}
		return false;
	}
	public void display()	//displaying data
	{
		System.out.println("Current Account.");
		System.out.println("Account no: " + accNo);
		System.out.println("Balance: " + balance);
		System.out.println("Overdraft limit: " + overdraftLimit);
		System.out.print("\n");
	}
}



class Filebanking			//main class
{
	public static void main(String []args)		//main function
	{
		Scanner input = new Scanner(System.in);
		double no=1516000, accountNo, overdraft=0, bal, money;	//here account no has been set to some default value therefore it generates automatic account no for different customers
		int i=0, k, j=0, s1, c1, l, monthX;
		byte flag=1, x1, choice, o, x2;
		float roi=0;
		String accountNoX, accountNo2;
		System.out.print("How many savings accounts do you want to keep? ");
		s1 = input.nextInt();
		System.out.print("How many current accounts do you want to keep? ");
		c1 = input.nextInt();
		Savings []s = new Savings[s1];		//creating reference of array of objects for Savings account
		Current []c = new Current[c1];
		Account []a = new Account[s1+c1];	//since Account is an abstract class therfore only it's reference can be created and we can't instantiate it
		do
		{
			System.out.print("\n0. Terminate \n1. Add Savings Account \n2. Add Current Account \n3. Search Specific Account \n4. Withdrawal/Deposit \n5. Delete specific account \n\n");
			System.out.print("Enter your choice: ");
			choice = input.nextByte();
			switch(choice)
			{
				case 1:		//for adding savings account
				if(i>s1-1)	//here whenever savings account is created i is incremented so i can go upto only (s1-1)
				{
					System.out.println("You can't add more savings account.");
				}
				else
				{
					no++;
					System.out.println("Your account no is: " + no);
					for(k=0; ; k++)	//to check condition of balance since balance has to be kept at a min of 1000rs for savings account
					{
						System.out.print("How much balance do you want to keep? ");
						bal = input.nextDouble();
						if(bal>=1000)
						{
							break;
						}
						else
						{
							System.out.println("Balance has to be at least 1000rs for opening savings account.");
						}
					}
					s[i] = new Savings(no, bal, roi);	//creating object of Savings class by calling it's parametrised Constructor
					for(k=0; ; k++)	//to check condition of rate of interest which has to be greater than 0
					{
						System.out.print("Enter rate of interest: ");
						roi = input.nextFloat();
						if(s[i].set_rate(roi))
						{
							break;
						}
						else
						{
							System.out.println("Rate of interest must be greater than 0.");
						}
					}
					for(k=0; ; k++)	//to check condition of month which has to be greater than 0
					{
						System.out.print("Enter the tenure of your savings(in months): ");
						monthX = input.nextInt();
						if(s[i].interestCalculate(monthX))
						{
							break;
						}
						else
						{
							System.out.println("Month should be at least 1.");
						}
					}
					System.out.println("Account sccessfully created.\n");
					i++;
				}
				try	//try catch block for writig data to a file
				{
					String saccountNo = String.valueOf((int)no) + ".dat";	//file name in string format
					FileOutputStream fOut = new FileOutputStream(saccountNo);//opening file of entered account no
					ObjectOutputStream oOut = new ObjectOutputStream(fOut);	//creating object of ObjectOutputStream
					oOut.writeObject(s[i-1]);	//writing object of Savings Account to a terminal
					oOut.flush();	//flushing so that data from terminal gets written in file
					oOut.close();	//close the file
				}
				catch(Exception e)
				{
					System.out.println("Error.");
				}
				break;
				case 2:		//for adding current account
				if(j>c1-1)	//to  check condition that current account does not exceed the size that is allocated at runtine in the variable c1
				{
					System.out.println("You can't add more current account.\n");
				}
				else
				{
					no++;	//automatic account no generator
					System.out.println("Your acc no is: " + no);
					for(k=0; ; k++)	//check balance is not less than 0 for current account
					{
						System.out.print("How much balance do you want to keep? ");
						bal = input.nextDouble();
						if(bal>0)
						{
							break;
						}
						else
						{
							System.out.println("Balance must be greater than 0 for current account.");
						}
					}
					c[j] = new Current(no, bal, overdraft);	//creating object of current account
					for(k=0; ; k++)		//to check overdraft limit is between "500" and "1Cr"rs for current account
					{
						System.out.print("Enter overdraft limit: ");
						overdraft = input.nextDouble();
						if(c[j].set_overdraft(overdraft))
						{
							break;
						}
						else
						{
							System.out.println("Overdraft limit must be at least 500rs.");
						}
					}
					System.out.println("Account sccessfully created.\n");
					j++;
				}
				try	//try catch block for writing data to a file
				{
					String saccountNo = String.valueOf((int)no) + ".dat";
					FileOutputStream fOut = new FileOutputStream(saccountNo);
					ObjectOutputStream oOut = new ObjectOutputStream(fOut);
					oOut.writeObject(c[j-1]);
					oOut.flush();
					oOut.close();
				}
				catch(Exception e)
				{
					System.out.println("Error.");
				}
				break;
				case 3:		//to view all types of account
				System.out.println("Which type of account do you want to see? ");
				System.out.println("1. Savings \n2. Current \n");
				System.out.print("Enter your choice: ");
				choice = input.nextByte();
				switch(choice)
				{
					case 1:	//to display Savings Account
					try
					{
						System.out.print("Enter account no of savings only(current not allowed): ");
						accountNoX = input.next();
						String saccountNoX = accountNoX + ".dat";
						FileInputStream fIn = new FileInputStream(saccountNoX);
						ObjectInputStream oIn = new ObjectInputStream(fIn);
						Savings sRef;	//Creating Savings Reference
						sRef = (Savings) oIn.readObject();	//typecasting so that i can display data in the form of display of 							Savings class
						sRef.display();		//displaying data from file
					}
					catch(Exception e)
					{
						System.out.println("Account not found.");
					}
					break;
					case 2:	//to display Current Account
					try
					{
						System.out.print("Enter account no of currents only(savings not allowed): ");
						accountNoX = input.next();
						String saccountNoX = accountNoX + ".dat";
						FileInputStream fIn = new FileInputStream(saccountNoX);
						ObjectInputStream oIn = new ObjectInputStream(fIn);
						Current cRef;
						cRef = (Current) oIn.readObject();
						cRef.display();
					}
					catch(Exception e)
					{
						System.out.println("Account not found.");
					}
					break;
				}
				break;
				case 4:	//for withdrawal and deposit
				System.out.println("From which type of account do you want to withdraw or deposit? ");
				System.out.println("1) SAVINGS \n2) CURRENT \n");
				System.out.print("Enter your choice: ");
				x1 = input.nextByte();
				if(x1==1)	//implementation of dynamic polymorphism(same as above which has been used in displaying accounts)
				{
					a=s;		//referring reference of class Account to object of Savings class
					l=i;
				}
				else
				{
					a=c;		//referring reference of class Account to object of Current class
					l=j;
				}
				System.out.println("1) WITHDRAW \n2) DEPOSIT");
				System.out.print("Enter your choice: ");
				x2 = input.nextByte();
				switch(x2)	//switch case for withdrawal and deposit
				{
					case 1:		//withdrawal
					System.out.print("Enter your acc no: ");
					accountNo = input.nextDouble();
					System.out.print("How much money do you want to withdraw? ");
					money = input.nextDouble();
					for(k=0; k<l; k++)
					{
						if(a[k].withdraw(money, accountNo))
						{
							try  //try catch block so that data gets updated on withdrawal
							{
								System.out.println("Successfull...");
								String saccountNoX = String.valueOf((int)accountNo) + ".dat";
								FileOutputStream fOut = new FileOutputStream(saccountNoX);
								ObjectOutputStream oOut = new ObjectOutputStream(fOut);
								if(x1==1)
								{
									oOut.writeObject(s[k]);
								}
								else
								{
									oOut.writeObject(c[k]);
								}
								oOut.flush();
								oOut.close();
								flag = 0;
							}
							catch(Exception e)
							{
								System.out.println("Account not found.");
							}
							break;
						}
					}
					if(flag==1)
					{
						System.out.println("Unsuccessful...");
					}
					break;
					case 2:		//deposit
					System.out.print("Enter your acc no: ");
					accountNo = input.nextDouble();
					System.out.print("How much money do you want to deposit? ");
					money = input.nextDouble();
					for(k=0; k<l; k++)
					{
						if(a[k].deposit(money, accountNo))
						{
							try	//try catch block so that data gets updated on deposit
							{
								System.out.println("Successfull...");
								String saccountNoX = String.valueOf((int)accountNo) + ".dat";
								FileOutputStream fOut = new FileOutputStream(saccountNoX);
								ObjectOutputStream oOut = new ObjectOutputStream(fOut);
								if(x1==1)
								{
									oOut.writeObject(s[k]);
								}
								else
								{
									oOut.writeObject(c[k]);
								}
								oOut.flush();
								oOut.close();
								flag = 0;
							}
							catch(Exception e)
							{
								System.out.println("Account not found.");
							}
							break;
						}
					}
					if(flag==1)
					{
						System.out.println("Unsuccessful...");
					}
					break;
				}
				break;
				case 5:	//for deleting the file
				try
				{
					System.out.print("Enter account no which you want to delete: ");
					accountNo2 = input.next();
					String saccountNo2 = accountNo2 + ".dat";
					File newFile = new File(saccountNo2);
					if(newFile.delete())	//library function delete that has return type boolean
					{
						System.out.println("Deleted Successfully.");
					}
					else
					{
						System.out.println("Unsuccessful.");
					}
				}
				catch(Exception e)
				{
					System.out.println("Account not found.");
				}
				break;
			}
		}while(choice!=0);
	}
}
