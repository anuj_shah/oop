import java.util.*;
import java.io.*;

abstract class Vehicle implements Serializable	//abstract class Vehicle implemented Serializable so that we can display it as per our format	//implemented the concept of containership which has "has a" relationship by creating object of engine class in Vehice class
{
	protected int idNo;
	private int seater;
	private String company;
	private String modelName;
	private String modelNo;
	private double price;
	private char gearChoice;
	private int noOfGears;
	private float mileage;
	private float topSpeed;
	private float fuelCapacity;
	private float length;
	private float breadth;
	private float width;
	private String licensePlate;
	private String color;
	private double weight;
	private String fuelType;
	Engine e = new Engine();	//creating object of engine class i.e containership
	Vehicle(int no)	//Constructor for setting reference no
	{
		idNo = no;
	}
	public void setData(int seaterY, String companyY, String modelNameY, String modelNoY, double priceY, char gearChoiceY, int noOfGearsY, float mileageY, float topSpeedY, float fuelCapacityY, float lengthY, float breadthY, float widthY, String licensePlateY, String colorY, double weightY, String fuelTypeY, String fuelSystemZ, float maxPowerZ, float maxTorqueZ, float displacementZ)	//setting data
	{
		seater = seaterY;
		company = companyY;
		modelName = modelNameY;
		modelNo = modelNoY;
		price = priceY;
		gearChoice = gearChoiceY;
		noOfGears = noOfGearsY;
		mileage = mileageY;
		topSpeed = topSpeedY;
		fuelCapacity = fuelCapacityY;
		length = lengthY;
		breadth = breadthY;
		width = widthY;
		licensePlate = licensePlateY;
		color = colorY;
		weight = weightY;
		fuelType = fuelTypeY;
		e.setDataEngine(fuelSystemZ, maxPowerZ, maxTorqueZ, displacementZ);
	}
	public void putData()		//displaying data as per our choice
	{
		System.out.println("General Specs: ");
		System.out.println("Id No: " + idNo);
		System.out.println("Company: " + company);
		System.out.println("Vehicle name: " + modelName);
		System.out.println("Series no: " + modelNo);
		System.out.println("No of seats: " + seater);
		if(gearChoice == 'y')
		{
			System.out.println("No of gears: " + noOfGears);
		}
		else
		{
			System.out.println("No of gears: 0");
		}
		System.out.println("Mileage: " + mileage);
		System.out.println("Top speed: " + topSpeed);
		System.out.println("Fuel capacity: " + fuelCapacity);
		System.out.println("Dimensions: ");
		System.out.println("Length: " + length);
		System.out.println("Breadth: " + breadth);
		System.out.println("Width: " + width);
		System.out.println("License plate: " + licensePlate);
		System.out.println("Color: " + color);
		System.out.println("Braking system: ");
		System.out.println("Total weight: " + weight);
		System.out.println("Fuel Type: " + fuelType);
		System.out.println("\nEngine specs: ");
		e.putData();		//displaying data of engine
	}
}

class Engine implements Serializable	//engine class not kept abstract since it's object is created in Vehice class
{
	private String fuelSystem;
	private float maxPower;
	private float maxTorque;
	private float displacement;
	public void setDataEngine(String fuelSystemY, float maxPowerY, float maxTorqueY, float displacementY) //setting data
	{
		fuelSystem = fuelSystemY;
		maxPower = maxPowerY;
		maxTorque = maxTorqueY;
		displacement = displacementY;
	}
	public void putData()	//displaying data
	{
		System.out.println("Fuel System: " + fuelSystem);
		System.out.println("Maximum Power: " + maxPower);
		System.out.println("Maximum Torque: " + maxTorque);
		System.out.println("Displacement: " + displacement);
	}
}

class TwoWheeler extends Vehicle implements Serializable	//child class of Vehicle
{
	private char helmetChoice;
	private String helmetCompany;
	TwoWheeler(int no)	//Constructor
	{
		super(no);
	}
	public void setData(int seaterY, String companyY, String modelNameY, String modelNoY, double priceY, char gearChoiceY, int noOfGearsY, float mileageY, float topSpeedY, float fuelCapacityY, float lengthY, float breadthY, float widthY, String licensePlateY, String colorY, double weightY, String fuelTypeY, String fuelSystemZ, float maxPowerZ, float maxTorqueZ, float displacementZ, char helmetChoiceY, String helmetCompanyY)	//setting data
	{
		//calling setData of parent class
		super.setData(seaterY, companyY, modelNameY, modelNoY, priceY, gearChoiceY, noOfGearsY, mileageY, topSpeedY, fuelCapacityY, lengthY, breadthY, widthY, licensePlateY, colorY, weightY, fuelTypeY, fuelSystemZ, maxPowerZ, maxTorqueZ, displacementZ);
		helmetChoice = helmetChoiceY;
		helmetCompany = helmetCompanyY;
	}
	public void putData()	//displaying data
	{
		super.putData();	//calling setData of parent class
		if(helmetChoice == 'y')
		{
			System.out.println("Helmet		: Yes");
			System.out.println("Company		: " + helmetCompany);
		}
		else
		{
			System.out.println("Helmet: No");
		}
	}
}

abstract class FourWheeler extends Vehicle implements Serializable	//abstract class FourWheeler
{
	private int noOfAirbags;
	private boolean wifi;
	private boolean gps;
	private boolean sensorLock;
	private char speakerChoice;
	private String speakerType;
	FourWheeler(int no)	//Constructor
	{
		super(no);
	}
	//setting data
	public void setData(int seaterY, String companyY, String modelNameY, String modelNoY, double priceY, char gearChoiceY, int noOfGearsY, float mileageY, float topSpeedY, float fuelCapacityY, float lengthY, float breadthY, float widthY, String licensePlateY, String colorY, double weightY, String fuelTypeY, String fuelSystemZ, float maxPowerZ, float maxTorqueZ, float displacementZ, int noOfAirbagsY, boolean wifiY, boolean gpsY, boolean sensorLockY, char speakerChoiceY, String speakerTypeY)
	{
		super.setData(seaterY, companyY, modelNameY, modelNoY, priceY, gearChoiceY, noOfGearsY, mileageY, topSpeedY, fuelCapacityY, lengthY, breadthY, widthY, licensePlateY, colorY, weightY, fuelTypeY, fuelSystemZ, maxPowerZ, maxTorqueZ, displacementZ);
		noOfAirbags = noOfAirbagsY;
		wifi = wifiY;
		gps = gpsY;
		sensorLock = sensorLockY;
		speakerType = speakerTypeY;
	}

	public void putData()	//displaying data
	{
		super.putData();	//calling putData of parent class
		System.out.println("No of Air bags: " + noOfAirbags);
		//below some are boolean datatypes so displaying in that format
		if(wifi)
		{
			System.out.println("Wifi: Yes");
		}
		else
		{
			System.out.println("Wifi: No");
		}
		if(gps)
		{
			System.out.println("GPS: Yes");
		}
		else
		{
			System.out.println("GPS: No");
		}
		if(sensorLock)
		{
			System.out.println("Sensor lock: Yes");
		}
		else
		{
			System.out.println("Sensor lock: No");
		}
		if(speakerChoice == 'y' || speakerChoice == 'Y')
		{
			System.out.println("Speakers: Yes");
			System.out.println("Speaker Type: " + speakerType);
		}
		else
		{
			System.out.println("Speakers: No");
		}
	}
}

class PrivateCar extends FourWheeler implements Serializable	//child class of FourWheeler
{
	PrivateCar(int no)	//Constructor
	{
		super(no);
	}
	//setting data
	public void setData(int seaterY, String companyY, String modelNameY, String modelNoY, double priceY, char gearChoiceY, int noOfGearsY, float mileageY, float topSpeedY, float fuelCapacityY, float lengthY, float breadthY, float widthY, String licensePlateY, String colorY, double weightY, String fuelTypeY, String fuelSystemZ, float maxPowerZ, float maxTorqueZ, float displacementZ, int noOfAirbagsY, boolean wifiY, boolean gpsY, boolean sensorLockY, char speakerChoiceY, String speakerTypeY)
	{
		super.setData(seaterY, companyY, modelNameY, modelNoY, priceY, gearChoiceY, noOfGearsY, mileageY, topSpeedY, fuelCapacityY, lengthY, breadthY, widthY, licensePlateY, colorY, weightY, fuelTypeY, fuelSystemZ, maxPowerZ, maxTorqueZ, displacementZ, noOfAirbagsY, wifiY, gpsY, sensorLockY, speakerChoiceY, speakerTypeY);
	}
	public void putData()	//displaying data
	{
		super.putData();
	}
}

class CommercialCar extends FourWheeler implements Serializable	//child class of FourWheeler
{
	CommercialCar(int no)	//Constructor
	{
		super(no);
	}
	//setting data
	public void setData(int seaterY, String companyY, String modelNameY, String modelNoY, double priceY, char gearChoiceY, int noOfGearsY, float mileageY, float topSpeedY, float fuelCapacityY, float lengthY, float breadthY, float widthY, String licensePlateY, String colorY, double weightY, String fuelTypeY, String fuelSystemZ, float maxPowerZ, float maxTorqueZ, float displacementZ, int noOfAirbagsY, boolean wifiY, boolean gpsY, boolean sensorLockY, char speakerChoiceY, String speakerTypeY)
	{
		super.setData(seaterY, companyY, modelNameY, modelNoY, priceY, gearChoiceY, noOfGearsY, mileageY, topSpeedY, fuelCapacityY, lengthY, breadthY, widthY, licensePlateY, colorY, weightY, fuelTypeY, fuelSystemZ, maxPowerZ, maxTorqueZ, displacementZ, noOfAirbagsY, wifiY, gpsY, sensorLockY, speakerChoiceY, speakerTypeY);
	}
	public void putData()	//displaying data
	{
		super.putData();
	}
}

class VehicleData	//main class
{
	public static void main(String args[])	//main function
	{
		Scanner input = new Scanner(System.in);
		//different input variables
		int seaterX, noOfGearsX, noOfAirbagsX, idNumberZ=0;
		double priceX, weightX;
		char gearChoiceX, helmetChoiceX, speakerChoiceX;
		float mileageX, topSpeedX, lengthX, breadthX, widthX, fuelCapacityX, maxPowerX, maxTorqueX, displacementX;
		String companyX, modelNameX, modelNoX, licensePlateX, colorX, fuelTypeX, fuelSystemX, helmetCompanyX, speakerTypeX;
		boolean wifiX, gpsX, sensorLockX;
		byte choice, choiceX, choiceZ;
		int idNumber = 1516000;

		do
		{
			System.out.print("\n0. Terminate \n1. Buy two Wheeler \n2. Buy private car \n3. Buy commercial car \n4. Search your records \n5. Delete records \n6. Update records \n\n");
			choice = input.nextByte();
			if(choice!=0 && (choice==1 || choice==2 || choice==3))	//if condition so that when user inputs 0 then it doesn't start taking input
			//here i have not used switch case since some general specs are common in TwoWheeler, PrivateCar and CommercialCar and their inputs are also common so to avoid duplication of inputs i have not used switch case
			{
				idNumber++;
				System.out.println("Your id no is " + idNumber);
				System.out.print("How much seater vehicle do you want? ");
				seaterX = input.nextInt();
				System.out.print("Enter the name of the company: ");
				companyX = input.next();
				System.out.print("Enter model name: ");
				modelNameX = input.next();
				System.out.print("Enter model no: ");
				modelNoX = input.next();
				System.out.print("Enter price: ");
				priceX = input.nextDouble();
				System.out.print("Do you want a vehicle with gear or without gear?(y/n) ");
				gearChoiceX = input.next().charAt(0);
				if(gearChoiceX == 'y')
				{
					System.out.print("Enter no of gears: ");
					noOfGearsX = input.nextInt();
				}
				else
				{
					gearChoiceX = 0;
					noOfGearsX = 0;
				}
				System.out.print("How much mileage do you want? ");
				mileageX = input.nextFloat();
				System.out.print("How much top speed do you want? ");
				topSpeedX = input.nextFloat();
				System.out.print("How much fuel capacity do you want? ");
				fuelCapacityX = input.nextFloat();
				System.out.println("What are the dimensions of your vehicle? ");
				System.out.print("Enter length: ");
				lengthX = input.nextFloat();
				System.out.print("Enter breadth: ");
				breadthX = input.nextFloat();
				System.out.print("Enter width: ");
				widthX = input.nextFloat();
				System.out.print("Enter license plate of your vehicle: ");
				licensePlateX = input.next();
				System.out.print("Which color do you want? ");
				colorX = input.next();
				System.out.println("What type of braking system do you want? ");
				System.out.print("How much weight of vehicle do you want? ");
				weightX = input.nextDouble();
				System.out.print("Which type of fuel do you want? ");
				fuelTypeX = input.next();
				System.out.print("Which type of fuel System do you want? ");
				fuelSystemX = input.next();
				System.out.print("What is the maximum power you want? ");
				maxPowerX = input.nextFloat();
				System.out.print("What is the maximum torque you want? ");
				maxTorqueX = input.nextFloat();
				System.out.print("What is the maximum displacement you want? ");
				displacementX = input.nextFloat();
				if(choice == 1)	//if choice is 1 then object of two wheeler would be created
				{
					TwoWheeler tw = new TwoWheeler(idNumber);	//creating object of two wheeler
					//some extra inputs which are required for TwoWheeler
					System.out.print("Do you want a vehicle with helmet?(y/n) ");
					helmetChoiceX = input.next().charAt(0);
					if(helmetChoiceX == 'y')
					{
						System.out.print("Which company's helmet do you want? ");
						helmetCompanyX = input.next();
					}
					else
					{
						helmetChoiceX = 0;
						helmetCompanyX = null;
					}
					//Calling setData of TwoWheeler
					tw.setData(seaterX, companyX, modelNameX, modelNoX, priceX, gearChoiceX, noOfGearsX, mileageX, topSpeedX, fuelCapacityX, lengthX, breadthX, widthX, licensePlateX, colorX, weightX, fuelTypeX, fuelSystemX, maxPowerX, maxTorqueX, displacementX);
					try	//try catch block for writing data to a binary file
					{
						String sidNo = String.valueOf(idNumber) + ".dat";	//filename in String format
						FileOutputStream fOut = new FileOutputStream(sidNo);	//creating object of FileOutputStream
						ObjectOutputStream oOut = new ObjectOutputStream(fOut);	//creating object of ObjectOutputStream
						oOut.writeObject(tw);	//writing object to a file
						oOut.flush();		//flushing so that data writes to a file
						oOut.close();		//closing the file
					}
					catch(Exception e)
					{
						System.out.println("Error.");
					}
				}
				if(choice == 2 || choice == 3)	//if choice is 2 then creating reference of FourWheeler
				{
					FourWheeler fw = new CommercialCar(idNumber);	//initializing reference of FourWheeler class with the object of CommercialCar
					//implementation of Dynamic Polymorphism
					if(choice == 2)	//choice 2 for referring to PrivateCar
					{
						fw = new PrivateCar(idNumber);
					}
					if(choice == 3)	//choice 3 for referring to CommercialCar
					{
						fw = new CommercialCar(idNumber);
					}
					//taking some extra inputs for FourWheeler
					System.out.print("Enter no of air bags you want? ");
					noOfAirbagsX = input.nextInt();
					System.out.print("Do you want wifi connectivity?(true/false) ");
					wifiX = input.nextBoolean();
					System.out.print("Do you want gps connectivity?(true/false) ");
					gpsX = input.nextBoolean();
					System.out.print("Do you want sensor lock?(true/false) ");
					sensorLockX = input.nextBoolean();
					System.out.print("Do you want speaker facility?(true/false) ");
					speakerChoiceX = input.next().charAt(0);
					if(speakerChoiceX == 'y')
					{
						System.out.print("Which company's speaker do you want? ");
						speakerTypeX = input.next();
					}
					else
					{
						speakerChoiceX = 0;
						speakerTypeX = null;
					}
					//calling setData function
					fw.setData(seaterX, companyX, modelNameX, modelNoX, priceX, gearChoiceX, noOfGearsX, mileageX, topSpeedX, fuelCapacityX, lengthX, breadthX, widthX, licensePlateX, colorX, weightX, fuelTypeX, fuelSystemX, maxPowerX, maxTorqueX, displacementX, noOfAirbagsX, wifiX, gpsX, sensorLockX, speakerChoiceX, speakerTypeX);
					try	//try catch block for writing input to a binary file
					{
						String sidNo = String.valueOf(idNumber) + ".dat";
						FileOutputStream fOut = new FileOutputStream(sidNo);
						ObjectOutputStream oOut = new ObjectOutputStream(fOut);
						oOut.writeObject(fw);
						oOut.flush();
						oOut.close();
					}
					catch(Exception e)
					{
						System.out.println("Error.");
					}
				}
			}
			if(choice == 4)	//if choice is 4 then it displays record which you want
			{
				System.out.println("Which type of vehicle have you purchased? ");
				System.out.print("1. Two Wheeler \n2. Private Car \n3. Commercial Car \n\n");
				System.out.print("Enter your choice: ");
				choiceX = input.nextByte();
				switch(choiceX)
				{
					case 1:	//for displaying data of TwoWheeler
					System.out.print("Enter your id no: ");
					String idNoZ = input.next();
					String sidNoZ = idNoZ + ".dat";
					try	//try catch block for writing data to a binary file
					{
						TwoWheeler twRef;	//creating TwoWheeler reference
						FileInputStream fIn = new FileInputStream(sidNoZ);	//opening the binary file
						ObjectInputStream oIn = new ObjectInputStream(fIn);	//creating object of ObjectOutputStream
						twRef = (TwoWheeler) oIn.readObject();	//typecasting to display data of TwoWheeler
						twRef.putData();	//display data
					}
					catch(Exception e)
					{
						System.out.println("Id not found.");
					}
					System.out.print("\n");
					break;
					case 2:	//for displaying data of PrivateCar
					System.out.print("Enter your id no: ");
					String idNoZ1 = input.next();
					String sidNoZ1 = idNoZ1 + ".dat";
					try	//try catch block for displaying data of PrivateCar
					{
						PrivateCar pcRef;	//creating PrivateCar reference
						FileInputStream fIn = new FileInputStream(sidNoZ1);
						ObjectInputStream oIn = new ObjectInputStream(fIn);
						pcRef = (PrivateCar) oIn.readObject();
						pcRef.putData();
					}
					catch(Exception e)
					{
						System.out.println("Id not found.");
					}
					System.out.print("\n");
					break;
					case 3:	//displaying data of CommercialCar
					System.out.print("Enter your id no: ");
					String idNoZ2 = input.next();
					String sidNoZ2 = idNoZ2 + ".dat";
					try
					{
						CommercialCar ccRef;  //creating CommercialCar reference
						FileInputStream fIn = new FileInputStream(sidNoZ2);
						ObjectInputStream oIn = new ObjectInputStream(fIn);
						ccRef = (CommercialCar) oIn.readObject();
						ccRef.putData();
					}
					catch(Exception e)
					{
						System.out.println("Id not found.");
					}
					System.out.print("\n");
					break;
				}
			}
			if(choice == 5)	//choice 5 for deleting data
			{
				System.out.print("Enter your id no: ");
				String idNoZ3 = input.next();
				String sidNoZ3 = idNoZ3 + ".dat";
				try	//try catch block for deleting file
				{
					File newFile = new File(sidNoZ3);
					if(newFile.delete())	//library function delete that has boolean datatype
					{
						System.out.println("Deleted Successfully.");
					}
					else
					{
						System.out.println("Unsuccessful...");
					}
				}
				catch(Exception e)
				{
					System.out.println("Id not found.");
				}
				System.out.print("\n");
			}
			if(choice==6)	//choice 6 for upgradation of records or for creating new file if that data doesn't exist
			{
				System.out.println("Note: If your ref no wouldn't exist then it would create data of new ref No.");
				System.out.print("Enter your ref no: ");
				idNumberZ = input.nextInt();
				System.out.println("What type of vehicle have you purchased? ");
				System.out.println("1. Two Wheeler \n2. Private Car \n3. Commercial Car \n\n");
				System.out.print("Enter your choice: ");
				choiceZ = input.nextByte();
				System.out.print("How much seater vehicle do you want? ");
				seaterX = input.nextInt();
				System.out.print("Enter the name of the company: ");
				companyX = input.next();
				System.out.print("Enter model name: ");
				modelNameX = input.next();
				System.out.print("Enter model no: ");
				modelNoX = input.next();
				System.out.print("Enter price: ");
				priceX = input.nextDouble();
				System.out.print("Do you want a vehicle with gear or without gear?(y/n) ");
				gearChoiceX = input.next().charAt(0);
				if(gearChoiceX == 'y')
				{
					System.out.print("Enter no of gears: ");
					noOfGearsX = input.nextInt();
				}
				else
				{
					gearChoiceX = 0;
					noOfGearsX = 0;
				}
				System.out.print("How much mileage do you want? ");
				mileageX = input.nextFloat();
				System.out.print("How much top speed do you want? ");
				topSpeedX = input.nextFloat();
				System.out.print("How much fuel capacity do you want? ");
				fuelCapacityX = input.nextFloat();
				System.out.println("What are the dimensions of your vehicle? ");
				System.out.print("Enter length: ");
				lengthX = input.nextFloat();
				System.out.print("Enter breadth: ");
				breadthX = input.nextFloat();
				System.out.print("Enter width: ");
				widthX = input.nextFloat();
				System.out.print("Enter license plate of your vehicle: ");
				licensePlateX = input.next();
				System.out.print("Which color do you want? ");
				colorX = input.next();
				System.out.println("What type of braking system do you want? ");
				System.out.print("How much weight of vehicle do you want? ");
				weightX = input.nextDouble();
				System.out.print("Which type of fuel do you want? ");
				fuelTypeX = input.next();
				System.out.print("Which type of fuel System do you want? ");
				fuelSystemX = input.next();
				System.out.print("What is the maximum power you want? ");
				maxPowerX = input.nextFloat();
				System.out.print("What is the maximum torque you want? ");
				maxTorqueX = input.nextFloat();
				System.out.print("What is the maximum displacement you want? ");
				displacementX = input.nextFloat();
				if(choiceZ == 1)	//choice 1 for upgradation of TwoWheeler
				{
					TwoWheeler tw = new TwoWheeler(idNumberZ);
					System.out.print("Do you want a vehicle with helmet?(y/n) ");
					helmetChoiceX = input.next().charAt(0);
					if(helmetChoiceX == 'y')
					{
						System.out.print("Which company's helmet do you want? ");
						helmetCompanyX = input.next();
					}
					else
					{
						helmetChoiceX = 0;
						helmetCompanyX = null;
					}
					tw.setData(seaterX, companyX, modelNameX, modelNoX, priceX, gearChoiceX, noOfGearsX, mileageX, topSpeedX, fuelCapacityX, lengthX, breadthX, widthX, licensePlateX, colorX, weightX, fuelTypeX, fuelSystemX, maxPowerX, maxTorqueX, displacementX);
					try	//try catch block for overwriting new data over old data in a file
					{
						String sidNo = String.valueOf(idNumberZ) + ".dat";
						FileOutputStream fOut = new FileOutputStream(sidNo);
						ObjectOutputStream oOut = new ObjectOutputStream(fOut);
						oOut.writeObject(tw);
						oOut.flush();
						oOut.close();
					}
					catch(Exception e)
					{
						System.out.println("Error.");
					}
				}
				if(choiceZ == 2 || choiceZ == 3)	//choice 2 and 3 for PrivateCar and CommercialCar respectively
				{
					FourWheeler fw = new CommercialCar(idNumberZ);	//initialising reference of FourWheeler with a object of CommercialCar
					if(choiceZ == 2)	//choice 2 for referring to PrivateCar
					{
						fw = new PrivateCar(idNumberZ);
					}
					if(choiceZ == 3)	//choice 3 for referring to CommercialCar
					{
						fw = new CommercialCar(idNumberZ);
					}
					System.out.print("Enter no of air bags you want? ");
					noOfAirbagsX = input.nextInt();
					System.out.print("Do you want wifi connectivity?(true/false) ");
					wifiX = input.nextBoolean();
					System.out.print("Do you want gps connectivity?(true/false) ");
					gpsX = input.nextBoolean();
					System.out.print("Do you want sensor lock?(true/false) ");
					sensorLockX = input.nextBoolean();
					System.out.print("Do you want speaker facility?(true/false) ");
					speakerChoiceX = input.next().charAt(0);
					if(speakerChoiceX == 'y')
					{
						System.out.print("Which company's speaker do you want? ");
						speakerTypeX = input.next();
					}
					else
					{
						speakerChoiceX = 0;
						speakerTypeX = null;
					}
					//implementation of dynamic polymorhism for setting data
					fw.setData(seaterX, companyX, modelNameX, modelNoX, priceX, gearChoiceX, noOfGearsX, mileageX, topSpeedX, fuelCapacityX, lengthX, breadthX, widthX, licensePlateX, colorX, weightX, fuelTypeX, fuelSystemX, maxPowerX, maxTorqueX, displacementX, noOfAirbagsX, wifiX, gpsX, sensorLockX, speakerChoiceX, speakerTypeX);
					try	//try catch block for overwriting new data over old data to a file
					{
						String sidNo = String.valueOf(idNumberZ) + ".dat";
						FileOutputStream fOut = new FileOutputStream(sidNo);
						ObjectOutputStream oOut = new ObjectOutputStream(fOut);
						oOut.writeObject(fw);
						oOut.flush();
						oOut.close();
					}
					catch(Exception e)
					{
						System.out.println("Error.");
					}
				}
			}
		}while(choice!=0);
	}
}
