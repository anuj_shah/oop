import java.util.Scanner;

class Matrix			//Matrix class
{
	Scanner input = new Scanner(System.in);
	public int rows;
	public int columns;
	public int [][]matrix;			//two dimensional array for storing values of matrix whose size will be allocated at run-time
	
	public Matrix(int m, int n)		//Matrix constructor with int as both the parameters
	{	
		if(m<1 || n<1)
		{
			System.out.println("Error allocating memory ");
		}
		else
		{
			rows = m;
			columns = n;
			matrix = new int[rows][columns];
		}
	}
	
	public void add(Matrix x)		//member function for adding two matrices
	{
		Matrix n1 = new Matrix(rows, columns);		//allocating memory to the matrix n1 which would be used for addition
		if(rows == x.rows && columns == x.columns)		//checking validity of matrices for addition
		{
			for(int i=0; i<rows; i++)	//loop for adding two matrices
			{
				for(int j=0; j<columns; j++)
				{
					n1.matrix[i][j] = matrix[i][j] + x.matrix[i][j];
				}
			}
			n1.putData();
		}	
		else
		{
			System.out.println("Matrix cannot be added ");
		}
	}
	
	public void multiply(Matrix y)			//matrix multiplication
	{
		int sum=0;
		Matrix m = new Matrix(rows, y.columns);		//allocating memory to the matrix m which would be used for multiplication
		if(columns == y.rows)					//checking validity for matrix multiplication
		{
			for(int i=0; i<rows; i++)			//loop for multiplication of matrix
			{
				for(int j=0; j<y.columns; j++)
				{
					for(int k=0; k<y.rows; k++)
					{
						sum += matrix[i][k] * y.matrix[k][j];
					}
					m.matrix[i][j] = sum;
					sum=0;
				}
			}
			m.putData();
		}
		else
		{
			System.out.println("Matrix cannot be multiplied ");
		}
	}
	
	public void multiplyScalar(int m)			//scalar multiplication of matrix
	{
		Matrix n = new Matrix(rows, columns);	//allocating memory to the matrix n for multiplication
		for(int i=0; i<rows; i++)
		{
			for(int j=0; j<columns; j++)
			{
				n.matrix[i][j] = m*matrix[i][j];
			}
		}
		n.putData();
	}
	
	public void transpose(int m)				//transpose of a matrix
	{
		if(m%2 == 0)					//if we do the transpose of a matrix even no of times then the transposed matrix would be same as original one
		{
			Matrix n = new Matrix(rows, columns);		//allocating memory to the matrix n for transpose // here rows and columns would be same as that of original one
			for(int i=0; i<n.rows; i++)
			{
				for(int j=0; j<n.columns; j++)
				{
					n.matrix[i][j] = matrix[i][j];
				}
			}
			n.putData();
		}
		else		//if we do the transpose of a matrix odd no of times then the transposed matrix would be same as that of first time transpose
		{
			Matrix n = new Matrix(columns, rows);	//allocating memory to the matrix n for transpose //here rows and columns would be interchanged than that of original one
			for(int i=0; i<n.rows; i++)
			{
				for(int j=0; j<n.columns; j++)
				{
					n.matrix[i][j] = matrix[j][i];
				}
			}
			n.putData();
		}
	}
	
	public void extractData(int m, int n)		//to find particular record from matrix
	{
		if((m-1)<=(rows-1) && (n-1)<=(columns-1))
		{
			System.out.printf("pos[%d][%d] %d", m-1, n-1, matrix[m-1][n-1]); 
		}
		else
		{
			System.out.println("Values are out of matrix's domain ");
		}
	}
	
	public void inputData()				//input of matrix values
	{			
		for(int i=0; i<rows; i++)
		{
			for(int j=0; j<columns; j++)
			{
				System.out.printf("pos[%d][%d] ", i, j);
				matrix[i][j] = input.nextInt();
			}
		}
	}
	
	public void putData()			//displaying matrix values in the form of table
	{
		for(int i=0; i<rows; i++)
		{	
			for(int j=0; j<columns; j++)
			{	
				System.out.printf(" %d   ", matrix[i][j]);
			}
			System.out.print("\n");
		}
	}
}

class Operation
{
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);
		int row1, row2, column1, column2, i, j, x, no, k, r, c;
		Matrix m1, m2, m3;
		System.out.print("Enter no of rows for 1st matrix : ");		//no of rows for first matrix
		row1 = input.nextInt();
		System.out.print("Enter no of columns for 1st matrix : ");		//no of columns for first matrix
		column1 = input.nextInt();
		System.out.print("Enter no of rows for 2nd matrix : ");		//no of rows for second matrix
		row2 = input.nextInt();
		System.out.print("Enter no of columns for 2nd matrix : ");		//no of columns for second matrix
		column2 = input.nextInt();
		m1 = new Matrix(row1, column1);						//allocating memory to the matrix m1 object of class Matrix
		m2 = new Matrix(row2, column2);						//allocating memory to the matrix m2 object of class Matrix
		m3 = new Matrix(row1, column1);						//allocating memory to the matrix m3 object of class Matrix
		System.out.print("\n");
		System.out.println("First Matrix : ");
		m1.inputData();
		System.out.print("\n");
		System.out.println("Second Matrix : ");
		m2.inputData();
		System.out.print("\n");
		System.out.println("Matrix 1 : ");
		m1.putData();
		System.out.print("\n");
		System.out.println("Matrix 2 : ");
		m2.putData();
		System.out.print("\n");
		do			//do while loop continues until we press 0
		{
			System.out.println("0) TERMINATE \n1) ADDITION \n2) MULTIPLICATION \n3) MULTIPLICATION WITH A SCALAR \n4) TRANSPOSE \n5) EXTRACT DATA \n");
			x = input.nextInt();
			System.out.print("\n");
			switch(x)
			{
				case 1:					//addition
				System.out.println("Addition : ");
				m1.add(m2);
				System.out.print("\n");
				break;
				case 2:					//multiplication
				System.out.println("Multiplication : ");
				m1.multiply(m2);
				System.out.print("\n");
				break;
				case 3:					//multiplication with a scalar
				System.out.println("Multiplication with a scalar : ");
				System.out.print("Which matrix do you want to multiply with a scalar? ");
				no = input.nextInt();
				if(no == 1)			//multiply first matrix with a scalar
				{
					System.out.print("Enter the scalar qty with which you want to multiply : ");
					k = input.nextInt();
					m1.multiplyScalar(k);	
				}
				else if(no == 2)		//multiply second matrix with a scalar
				{
					System.out.print("Enter the scalar qty with which you want to multiply : ");
					k = input.nextInt();
					m2.multiplyScalar(k);
				}
				else
				{
					System.out.println("Matrix doesn't exist ");
				}
				System.out.print("\n");
				break;
				case 4:				//transpose
				System.out.println("Transpose of a matrix : ");
				System.out.print("Which matrix do you want to transpose? ");
				no = input.nextInt();
				if(no == 1)		//transpose of first matrix
				{
					System.out.print("How many times do you want to transpose? ");	//transpose matrix 1 how many times
					k = input.nextInt();
					m1.transpose(k);
				}
				else if(no == 2)	//transpose of first matrix
				{
					System.out.print("How many times do you want to transpose? ");	//transpose matrix 2 how many times
					k = input.nextInt();
					m2.transpose(k);
				}
				else
				{
					System.out.println("Matrix doesn't exist ");
				}
				System.out.print("\n");
				break;
				case 5:			//extract data from matrix
				System.out.println("Extract Data : ");
				System.out.print("From which matrix do you want to extract data? ");
				no = input.nextInt();
				if(no == 1)		//extract data from first matrix
				{
					System.out.print("Enter row no : ");
					r = input.nextInt();
					System.out.print("Enter column no : ");
					c = input.nextInt();
					m1.extractData(r, c);
					System.out.print("\n");
				}
				else if(no == 2)	//extract data from second matrix
				{	
					System.out.print("Enter row no : ");
					r = input.nextInt();
					System.out.print("Enter column no : ");
					c = input.nextInt();
					m2.extractData(r, c);
					System.out.print("\n");
				}
				else
				{
					System.out.println("Matrix doesn't exist ");
				}
				System.out.print("\n");
				break;
			}
		}while(x!=0);
	}
}
