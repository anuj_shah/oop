#include<iostream>
#include<cstdlib>

using namespace std;

class Polynomial		//class Polynomial
{
	private:
	float *coefficient;
	int *degree;
	public:
	int size;
	Polynomial();
	Polynomial(int);
	//declaring friend functions for different operations
	friend istream & operator >>(istream &, Polynomial &);
	friend ostream & operator <<(ostream &, Polynomial &);
	friend Polynomial operator +(Polynomial, Polynomial);
	friend Polynomial operator *(int, Polynomial);
	friend Polynomial operator -(Polynomial);
};

Polynomial :: Polynomial()		//constructor
{
	size = 0;
}

Polynomial :: Polynomial(int m)		//constructor with int as parameter that would take the size of polynomial
{
	size = m;
	coefficient = new float[size];
	degree = new int[size];
}

istream & operator >>(istream &in, Polynomial &p)		//>> extraction operator for input directly through object
{
	int no, i, j, l=0, flag;
	for(i=0; i<p.size; i++)
	{
		cout<<"Enter the coefficient for term "<<i+1<<" : ";
		in>>p.coefficient[i];
		cout<<"Enter the degree for term "<<i+1<<" : ";
		in>>p.degree[i];
		cout<<"\n";
	}
	return in;
}

ostream & operator <<(ostream &out, Polynomial &p)	//<< insertion operator for output directly from object
{
	int i;
	for(i=0; i<p.size; i++)
	{
		if(i == p.size-1)
		{
			out<<p.coefficient[i]<<"x^"<<p.degree[i];	
		}
		else
		{
			out<<p.coefficient[i]<<"x^"<<p.degree[i]<<" + ";
		}
	}
	return out;
}

Polynomial operator +(Polynomial p1, Polynomial p2)		//operator + for adding two polynomials
{
	int i, k=0, j, flag;
	Polynomial p3(100);
	for(i=0; i<p1.size; i++)		//first compare the degree to add coefficients of same degree
	{
		for(j=0; j<p2.size; j++)			
		{
			if(p1.degree[i] == p2.degree[j])
			{
				p3.degree[k] = p1.degree[i];
				p3.coefficient[k] = p1.coefficient[i] + p2.coefficient[j];
				k++;
			}
		}
	}
	for(i=0; i<p1.size; i++)		//compare the degree of first polynomial with the degree of p3
	{	
		for(j=0, flag=1; j<p1.size; j++)
		{
			if(p1.degree[i] == p3.degree[j])
			{
				flag=0;
			}
		}
		if(flag != 0)			//if degree of polynomial p1 doesn't exist in p3 then flag!=0 and that degree would be stored in p3
		{
			p3.degree[k] = p1.degree[i];
			p3.coefficient[k] = p1.coefficient[i];
			k++;
		}
	}
	for(i=0; i<p2.size; i++)		//if degree of polynomial p2 doesn't exist in p3 then flag!=0 and that degree would be stored in p3
	{	
		for(j=0, flag=1; j<p2.size; j++)
		{
			if(p2.degree[i] == p3.degree[j])
			{
				flag=0;
			}
		}
		if(flag != 0)
		{
			p3.degree[k] = p2.degree[i];
			p3.coefficient[k] = p2.coefficient[i];
			k++;
		}
	}
	p3.size = k;		//providing size to p3 which would be exactly be same as the no of terms in polynomial p3
	return p3;
}

Polynomial operator *(int cons, Polynomial p1)		//* operator to multiply given polynomial with a constant
{	
	int i;
	Polynomial p2(p1.size);
	for(i=0; i<p1.size; i++)				//multiply polynomial with a scalar value
	{
		p2.coefficient[i] = cons*p1.coefficient[i];
		p2.degree[i] = p1.degree[i];
	}
	return p2;
}

Polynomial operator -(Polynomial p1)		//- operator to find negation of polynomial
{
	int i;
	Polynomial p2(p1.size);
	for(i=0; i<p1.size; i++)			//making the coefficients of polynomial negative
	{
		p2.coefficient[i] = -(p1.coefficient[i]);
		p2.degree[i] = p1.degree[i];
	}
	return p2;
}

int main()				//main function

{
	int no1, no2, x;
	cout<<"Enter no of terms in polynomial 1 : ";		//to enter size of polynomial p1 which would be passed as size of polynomial
	cin>>no1;
	cout<<"Enter no of terms in polynomial 2 : ";		//to enter size of polynomial p2 which would be passed as size of polynomial
	cin>>no2;
	cout<<"\n";
	Polynomial p1(no1), p2(no2);					//declaring objects and allocating size to the objects of Polynomial
	cin>>p1;									//taking input of polynomial p1 through >> operator	
	cout<<"Polynomial 1 : "<<p1<<endl<<"\n";		//displaying polynomial p1 through << operator	
	cin>>p2;									//taking input of polynomial p2 through >> operator	
	cout<<"Polynomial 2 : "<<p2<<endl<<"\n";		//displaying polynomial p2 through << operator	
	do										//do while loop continues until we press 0
	{		
		cout<<"0) TERMINATE \n1) ADDITION \n2) MULTIPLY WITH A CONSTANT \n3) NEGATION OF POLYNOMIAL \n\n";	
		cin>>x;
		switch(x)
		{
			case 1:				//addition of polynomial
			//whichever no would be larger it's size would be allocated to Polynomial "addP"
			if(no1 >= no2)			
			{
				Polynomial addP(no1);
				{
					addP = p1 + p2;
					cout<<"Result : "<<addP;
				}
			}
			else
			{
				Polynomial addP(no2);
				{
					addP = p1 + p2;
					cout<<"Result : "<<addP;
				}
			}
			cout<<"\n\n";
			break;
			case 2:				//multiplication
			int constant, choice;
			cout<<"Which polynomial do you want to multiply with a constant? ";
			cin>>choice;
			if(choice == 1)		//if choice is 1 then 1st polynomial would be multiplied with a constant
			{
				Polynomial p3(p1.size);
				cout<<"Enter the constant with which you want to multiply : ";
				cin>>constant;
				p3 = constant*p1;
				cout<<"\nRseult : "<<p3;
				cout<<"\n\n";
			}
			else if(choice == 2)	//if choice is 2 then 2nd polynomial would be multiplied with a constant
			{
				Polynomial p3(p2.size);
				cout<<"Enter the constant with which you want to multiply : ";
				cin>>constant;
				p3 = constant*p2;
				cout<<"\nResult : "<<p3;
				cout<<"\n\n";
			}
			else
			{
				cout<<"Polynomial doesn't exist \n\n";
			}
			break;
			case 3:				//negation
			cout<<"Enter the polynomial for which you want to do negation operation : ";
			cin>>choice;
			if(choice == 1)		//if choice is 1 then 1st polynomial would be negated
			{
				Polynomial p3(p1.size);
				p3 = -(p1);
				cout<<"\nRseult : "<<p3;
				cout<<"\n\n";
			}
			else if(choice == 2)	//if choice is 1 then 1st polynomial would be negated
			{
				Polynomial p3(p2.size);
				p3 = -(p2);
				cout<<"\nRseult : "<<p3;
				cout<<"\n\n";
			}
			else
			{
				cout<<"Polynomial doesn't exist \n\n";
			}
			break;
		}
	}while(x!=0);
}
