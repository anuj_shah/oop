import java.util.Scanner;

class Bank				//bank class
{
	Scanner input = new Scanner(System.in);
	String name;
	String phoneNo;
	String address;
	int acc_no;
	double bal;
	Bank()				//constructor for assigning acc no by default
	{
		bal=0;
	}
	void getData()			//to get data of customers
	{
		System.out.print("Enter your name : ");
		name = input.next();
		System.out.print("Enter your phone No : ");
		phoneNo = input.next();
		System.out.print("Enter your address : ");
		address = input.next();
		System.out.print("Enter acc no : ");
		acc_no = input.nextInt();
		System.out.print("\n");
	}
	void putData()			//to print data of customers
	{
		System.out.println("Name 			: " + name);
		System.out.println("Acc No 			: " + acc_no);
		System.out.println("Phone No 		: " + phoneNo);
		System.out.println("Address 		: " + address);
		System.out.println("Bal 			: " + bal);
	}
	
	void deleteData()		//to delete records
	{
		name = null;
		acc_no = 0;
		phoneNo = null;
		address = null;
		bal = 0;
	}
}

class Account
{
	public static void main(String args[])
	{
		int i=0, j=0, k=0, l=0, accNo, no, no1, count=0, size;
		double bal1;
		Scanner input1 = new Scanner(System.in);
		System.out.print("How many accounts do you want to keep? ");	//dynamically allocating memory at runtime
		size = input1.nextInt();
		Bank[] array = new Bank[size];		//creating array of class bank
		for(l=0; l<array.length; l++)		//assigning memory to objects of bank
		{
			array[l] = new Bank();		
		}
		do
		{
			System.out.println("1) ADD RECORDS \n2) SHOW PARTICULAR RECORDS \n3) CREDIT/DEBIT \n4) DELETE RECORDS \n5) SHOW ALL RECORDS \n6) DUPLICATE CHECKER AND DELETER \n7) SORT ASCENDING \n8) SORT DESCENDING \n9) REVERSE THE LIST \n10) PRINT DETAILS WITH MAX ACCOUNT NO \n11) PRINT DETAILS WITH MIN ACCOUNT NO \n0) TERMINATE\n");
			no = input1.nextInt();
			switch(no)			//switch case for various options
			{
				case 1:	
				if(i<size)
				{		//to get data
					array[i].getData();
					i++;
				}
				else
				{
					System.out.println("Stack is full \n");
				}
				count++;
				break;
				case 2:			//to show records
				System.out.print("Enter Account No : ");
				accNo = input1.nextInt();
				for(l=0; l<size; l++)	//to print records
				{	
					if(accNo == array[l].acc_no)
					{
						array[l].putData();
						System.out.print("\n");
						break;
					}
					else if(l==(size-1) && array[l].acc_no != accNo)
					{
						System.out.println("Account doesn't exist ");
						break;
					}
				}
				break;
				case 3:			//to credit or debit
				System.out.println("1) CERDIT \n2) DEBIT");
				no1 = input1.nextInt();
				switch(no1)		//switch case for credit/debit
				{
					case 1:		//for credit
					System.out.print("Enter Account No : ");
					accNo = input1.nextInt();
					for(k=0; k<50; k++)
					{
						if(accNo == array[k].acc_no)
						{
							System.out.print("How much amt do you want to credit? ");
							bal1 = input1.nextDouble();
							array[k].bal = array[k].bal + bal1;
							break;
						}
						else if(k==(size-1) && array[k].acc_no != accNo)
						{
							System.out.println("Account doesn't exist ");
							break;
						}
					}					
					break;
					case 2:		//for debit
					System.out.print("Enter Account No : ");
					accNo = input1.nextInt();
					for(k=0; k<size; k++)
					{
						if(accNo == array[k].acc_no)
						{
							System.out.print("How much amt do you want to debit? ");
							bal1 = input1.nextDouble();
							if(bal1 >= array[k].bal)
							{
								System.out.println("Amt exceeds amt in balance ");	
								break;	
							}
							else
							{
								array[k].bal = array[k].bal - bal1;
								break;
							}
						}
						else if(k==(size-1) && array[k].acc_no != accNo)
						{
							System.out.println("Account doesn't exist ");
							break;
						}
					}
					break;
				}
				break;
				case 4:				//deleting records
				System.out.print("Enter acc no to be removed : ");
				accNo = input1.nextInt();
				for(l=0; l<size; l++)		//loop for deleting
				{
					char ch;
					if(accNo == array[l].acc_no)
					{
						array[l].deleteData();
						System.out.print("Do you want to add data?(y/n) ");
						ch = input1.next().charAt(0);
						if(ch == 'y')
						{
							array[l].getData();
						}
						else
						{
							System.out.println("Your acc is deleted ");
						}
						break;
					}
					else if(l==(size-1) && array[l].acc_no != accNo)
					{
						System.out.println("Account doesn't exist ");
						break;
					}
				}
				break;
				case 5:				//to show all records at one time
				for(k=0; k<size; k++)
				{
					if(array[k].acc_no != 0)
					{
						array[k].putData();
						System.out.print("\n");
					}
				}
				break;	
				case 6:	     //to check whether dupicate records exist or not and if yes then it would delete the first one in the list
				for(k=0; k<size; k++)		
				{
					if(array[k].acc_no == array[k+1].acc_no)
					{
						array[k].deleteData();
						System.out.println("Duplicate acc has been deleted \n");
						break;
					}
					else if(k==(size-1) && array[k].acc_no != array[k+1].acc_no)
					{
						System.out.println("Dupicate acc doesn't exist \n");
					}
				}	
				break;	
				case 7:		//to sort the list in ascending manner
				String nametmp=null, phonetmp=null, addtmp=null;
				double baltmp=0;
				int temp=0;
				for(k=0; k<size; k++)		//sorting ascending
				{
					for(l=0; l<size-1; l++)
					{
						if(array[l].acc_no > array[l+1].acc_no)	
						{
							temp = array[l].acc_no;
							nametmp = array[l].name;
							phonetmp = array[l].phoneNo;
							addtmp = array[l].address;
							baltmp = array[l].bal;							
							array[l].acc_no = array[l+1].acc_no;
							array[l].name = array[l+1].name;
							array[l].phoneNo = array[l+1].phoneNo;
							array[l].address = array[l+1].address;
							array[l].bal = array[l+1].bal;
							array[l+1].acc_no = temp;
							array[l+1].name = nametmp;
							array[l+1].phoneNo = phonetmp;
							array[l+1].address = addtmp;
							array[l+1].bal = baltmp;
						}
					}
				}
				System.out.println("Sorting has been done \n");
				break;	
				case 8:			//to sort the list in descending manner
				String nametmp1=null, phonetmp1=null, addtmp1=null;
				double baltmp1=0;
				int temp1=0;
				for(k=0; k<size; k++)		//sorting descending
				{
					for(l=0; l<size-1; l++)
					{
						if(array[l].acc_no < array[l+1].acc_no)
						{
							temp1 = array[l].acc_no;
							nametmp1 = array[l].name;
							phonetmp1 = array[l].phoneNo;
							addtmp1 = array[l].address;
							baltmp1 = array[l].bal;							
							array[l].acc_no = array[l+1].acc_no;
							array[l].name = array[l+1].name;
							array[l].phoneNo = array[l+1].phoneNo;
							array[l].address = array[l+1].address;
							array[l].bal = array[l+1].bal;
							array[l+1].acc_no = temp1;
							array[l+1].name = nametmp1;
							array[l+1].phoneNo = phonetmp1;
							array[l+1].address = addtmp1;
							array[l+1].bal = baltmp1;
						}
					}
				}
				System.out.println("Sorting has been done \n");
				break;
				case 9:			//reversing the list
				for(i=size-1; i>-1; i--)
				{	if(array[i].acc_no != 0)
					{
						array[i].putData();
						System.out.print("\n");
					}
				}
				break;
				case 10:		//generate max account no with details
				int max, m=0;
				for(l=0, max = array[l].acc_no; l<size; l++)	//loop for checking max
				{
					if(array[l].acc_no>max)
					{
						max=array[l].acc_no;
						m=l;
					}	
				}
				array[m].putData();
				System.out.print("\n");
				break;
				case 11:		//generate min account no with details
				int min, n=0;		
				for(l=0, min = array[l].acc_no; l<size; l++)  //loop for checking min
				{
					if(array[l].acc_no<min && array[l].acc_no!=0)
					{
						min=array[l].acc_no;
						n=l;
					}	
				}
				array[n].putData();
				System.out.print("\n");
				break;
			}
		}while(no!=0);
	}
}
