import java.util.Scanner;

class Bank				//bank class
{
	Scanner input = new Scanner(System.in);
	String name;
	String phoneNo;
	String address;
	public
	static int count = 1;		//static variable for assignig it to account no
	int acc_no;
	double bal;
	Bank()				//constructor for assigning acc no by default
	{
		bal=0;
		acc_no = count;
		count = count+1;
	}
	void getData()			//to get data of customers
	{
		System.out.print("Enter your name : ");
		name = input.next();
		System.out.print("Enter your phone No : ");
		phoneNo = input.next();
		System.out.print("Enter your address : ");
		address = input.next();
		System.out.println("Your acc no is " + acc_no);
		System.out.print("\n");
	}
	void putData()			//to print data of customers
	{
		System.out.println("Name 		: " + name);
		System.out.println("Acc No 		: " + acc_no);
		System.out.println("Phone No 	: " + phoneNo);
		System.out.println("Address 	: " + address);
		System.out.println("Bal 		: " + bal);
	}
}

class Account
{
	public static void main(String args[])
	{
		int i=0, j=0, k=0, l=0, accNo, no, no1;
		double bal1;
		Scanner input1 = new Scanner(System.in);
		Bank[] array = new Bank[50];		//creating array of class bank
		for(l=0; l<array.length; l++)		//assigning memory to objects of bank
		{
			array[l] = new Bank();		
		}
		do
		{
			System.out.println("1) Add Records \n2) Show Records \n3) Credit/Debit \n0) Terminate\n");
			no = input1.nextInt();
			switch(no)			//switch case for various options
			{
				case 1:			//to get data
				array[i].getData();
				i++;
				break;
				case 2:			//to show records
				System.out.print("Enter Account No : ");
				accNo = input1.nextInt();
				for(i=0; i<50; i++)	//to print records
				{	
					if(accNo == array[i].acc_no)
					{
						array[i].putData();
						break;
					}
				}
				break;
				case 3:			//to credit or debit
				System.out.println("1) Credit \n2) Debit");
				no1 = input1.nextInt();
				switch(no1)		//switch case for credit/debit
				{
					case 1:		//for credit
					System.out.print("Enter Account No : ");
					accNo = input1.nextInt();
					for(k=0; k<50; k++)
					{
						if(accNo == array[k].acc_no)
						{
							System.out.print("How much amt do you want to credit? ");
							bal1 = input1.nextDouble();
							array[k].bal = array[k].bal + bal1;
							break;
						}
					}					
					break;
					case 2:		//for debit
					System.out.print("Enter Account No : ");
					accNo = input1.nextInt();
					for(k=0; k<50; k++)
					{
						if(accNo == array[k].acc_no)
						{
							System.out.print("How much amt do you want to debit? ");
							bal1 = input1.nextDouble();
							if(bal1 >= array[k].bal)
							{
								System.out.println("Amt exceeds amt in balance ");	
								break;	
							}
							else
							{
								array[k].bal = array[k].bal - bal1;
								break;
							}
						}
					}
					break;
				}
			}
		}while(no!=0);
	}
}
